import os
import cv2
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rn 
import pymagsac
from scipy.io import loadmat, savemat
import json


def verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON):
    ''' f1, f2 - kpts found by detector; matches - matches between f1 and f2 
        pvxA, pvxB - ground truth; EPSILON - acceptable match error '''
    
    numCorrMtchs = 0
    correctA = []
    correctB = []

    for i in range(matches.shape[1]):

        distsA = (pvxA[0,:]-f1[0,matches[0,i]])**2 + (pvxA[1,:]-f1[1,matches[0,i]])**2
        distsB = (pvxB[0,:]-f2[0,matches[1,i]])**2 + (pvxB[1,:]-f2[1,matches[1,i]])**2
        epsA = distsA < EPSILON**2
        epsB = distsB < EPSILON**2

        if np.logical_and(epsA, epsB).any():

            # test for duplicate matches
            distsMA = (f1[0,correctA] - f1[0,matches[0,i]])**2 + (f1[1,correctA] - f1[1,matches[0,i]])**2
            distsMB = (f2[0,correctB] - f2[0,matches[1,i]])**2 + (f2[1,correctB] - f2[1,matches[1,i]])**2
            epsMA = distsMA < 0.01**2
            epsMB= distsMB < 0.01**2
            
            dup = np.logical_and(epsMA, epsMB).any()

            if not dup:
                numCorrMtchs = numCorrMtchs + 1
                correctA = np.append(correctA, matches[0,i]).astype(np.uint32)
                correctB = np.append(correctB, matches[1,i]).astype(np.uint32)

    return np.stack((correctA, correctB)).astype(np.uint32)

def draw_matches_o(pvxA, pvxB, imgA, imgB, iR):
    imgAB = np.concatenate((imgA,imgB), axis=1)
    
    plt.axis('off')
    plt.imshow(imgAB)

    if iR.size == 0:
        return
   
    sA = imgA.shape
    
    plt.scatter(pvxA[0,iR[0,:]], pvxA[1, iR[0,:]], 0.5, c='blue', alpha=0.5)
    plt.scatter(pvxB[0, iR[1,:]] + sA[1], pvxB[1,iR[1,:]], 0.5, c='blue', alpha=0.5)
    plt.plot(np.stack((pvxA[0,iR[0,:]],pvxB[0,iR[1,:]]+sA[1])),
         np.stack((pvxA[1, iR[0,:]],pvxB[1,iR[1,:]])),
         c='limegreen',
         linewidth=0.7)
    plt.show()
    return

if __name__=='__main__':

    dataPath = 'X:\\CIIRC\\BP\\benchmark\\results\\HoloLens_hall_down'
    datasetPath = 'X:\\CIIRC\\BP\\benchmark\\HoloLens_hall_down'
    toMatch = loadmat(os.path.join(dataPath, 'toMatch.mat'))['toMatch']
    
    

    iA = toMatch[1,0]
    iB = toMatch[1,1]

    allIters = loadmat(os.path.join(dataPath, f'datI_{iA}_{iB}_noVer_r.mat'))

    print('a')
    imA = os.path.join(datasetPath, f'img{iA}.jpg')
    imB = os.path.join(datasetPath, f'img{iB}.jpg')
    kpA = os.path.join(datasetPath, f'ktps{iA}.mat')
    kpB = os.path.join(datasetPath, f'ktps{iB}.mat')

    outF = f'X:\\CIIRC\\BP\\benchmark\\results\\HoloLens_door\\i{iA}i{iB}_m.mat'

    pvxA = loadmat(kpA)['pvxA']
    pvxB = loadmat(kpB)['pvxB']

    imgA = cv2.cvtColor(cv2.imread(imA), cv2.COLOR_BGR2RGB)
    imgB = cv2.cvtColor(cv2.imread(imB), cv2.COLOR_BGR2RGB)

    sA = imgA.shape
    sB = imgB.shape
    i = 0
    numIs = allIters['a'].shape[0]
    allItersUpdated = np.array([])
    for itera in allIters['a']:
        print(f'Iteration {i} of {numIs}.')
        i += 1
        kptsA = itera[0][2]
        kptsB = itera[0][3]
        matches = itera[0][4]-1
        matchesLRV = itera[0][5]-1
        matchesLR = itera[0][6]-1

        maskMG = []
        if matches.shape[1] > 6:
            H, maskMG = pymagsac.findFundamentalMatrix(x1y1=kptsA[:, matches[0, :]].T, x2y2=kptsB[:, matches[1, :]].T, w1=sA[1], h1=sA[0], w2=sB[1], h2=sB[0])#, use_magsac_plus_plus=False, sigma_th=1, partition_num=8, max_iters=800, conf=0.98)

        matchesMG = matches[:, maskMG]
        matchesMGV = verifyMatches(matchesMG, kptsA, kptsB, pvxA, pvxB, 6)
        #draw_matches_o(kptsA, kptsB, imgA, imgB, matches)
        #draw_matches_o(kptsA, kptsB, imgA, imgB, matchesLR)
        #draw_matches_o(kptsA, kptsB, imgA, imgB, matchesLRV)
        #draw_matches_o(kptsA, kptsB, imgA, imgB, matchesMG)
        #draw_matches_o(kptsA, kptsB, imgA, imgB, matchesMGV)
        iterUpdated = np.reshape(np.array([itera[0][0].astype(np.float32), itera[0][1],itera[0][2], itera[0][3], itera[0][4], itera[0][5], itera[0][6], matchesMG, matchesMGV], dtype=object), [1,9])
        if allItersUpdated.size ==0:
            allItersUpdated = iterUpdated
        else:
            allItersUpdated = np.concatenate((allItersUpdated, iterUpdated))

        if i%50 == 0:
            print("Saving")
            savemat(outF,{'itersUpdated':allItersUpdated})
        
        print('Iteration finished')
        
            
            #iR = rn.randint(0, pvxA.shape[1], size=(1, 80)) if rand else np.arange(pvxA.shape[1])
            #print(f'orig: {spkM.shape[0]} - V: {spkMMV.shape[1]} /  MS: {spkMM.shape[0]} = {spkMMV.shape[1]/ spkMM.shape[0]*100}%')






#         draw_matches_o(pvxA, pvxB, imgA, imgB)

#         pkTs = linspace(0,21,22);
#         egTs = linspace(2,22,21);

# I1 = single(rgb2gray(imgA));
# I2 = single(rgb2gray(imgB));
# EPSILON = 6;

# % [srt_scr, idx] = sort(scores, 'ascend');
# % matches = matches(:,idx);
# fprintf('=> Starting data extraction for\n   %s, \n   %s, \n   with epsilon of %d.\n', imA, imB, EPSILON);

# allData.imA = imA;
# allData.imB = imB;
# allData.kpA = kpA;
# allData.kpB = kpB;
# allData.sA = sA;
# allData.sB = sB;
# allData.eps = EPSILON;
# allData.pkTs = pkTs;
# allData.egTs = egTs;

# allIters = size(pkTs, 2)*size(egTs, 2);
# currIter = 0;
# for iPkT=1:size(pkTs, 2)
#     pkT = pkTs(iPkT);
#     for iEgT = 1:size(egTs, 2)
#         egT = egTs(iEgT);
#         currIter = currIter+1;
#         fprintf(' > Iteration %d/%d with PE %d and ET %d\n', currIter, allIters, pkT, egT);
#         %feature extraction
#         [f1,d1] = vl_sift(I1);%,'PeakThresh', pkT, 'edgethresh', egT);
#         [f2,d2] = vl_sift(I2);%,'PeakThresh', pkT, 'edgethresh', egT);
#         [matches, scores] = vl_ubcmatch(d1, d2, 1.5);

#         verMatches = verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON);
        
#         iterData.pT = pkT;
#         iterData.eT = egT;
#         iterData.nMa = size(matches, 2);
#         iterData.nVMa = size(verMatches, 2);
#         iterData.ma = matches;
#         iterData.vMa = verMatches;
        
#         iterName = sprintf('iterP%dE%d', pkT, egT);
#         allData.iters.(iterName) = iterData;
#         figure();
#         imshow(img12); hold on;
        
#         matches = verMatches;
#         plot(f1(1,matches(1,:)), f1(2, matches(1,:)), 'b.', 'MarkerSize', 5)
#         plot((f2(1,matches(2,:)) + sA(2)), f2(2,matches(2,:)), 'b.', 'MarkerSize', 5)
#         for i = 1:numel(matches(1,:))
#             plot([f1(1,matches(1,i)) (f2(1,matches(2,i)) + sA(2))], [f1(2, matches(1,i)) f2(2,matches(2,i))], 'g', 'LineWidth', 0.4);
#         end
#     end
# end

# save(outF, '-struct', 'allData');
# fprintf('=> Data saved, process Finished.\n');


# function correctMatches = verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON)
#     numCorrMtchs = 0;
#     correctA = [];
#     correctB = [];
#     for i=1:size(matches, 2)
#         distsA = (pvxA(1,:)-f1(1,matches(1,i))).^2 + (pvxA(2,:)-f1(2,matches(1,i))).^2;
#         distsB = (pvxB(1,:)-f2(1,matches(2,i))).^2 + (pvxB(2,:)-f2(2,matches(2,i))).^2;
#         epsA = distsA < EPSILON^2;
#         epsB = distsB < EPSILON^2;
        
#         k = (epsA == 1 & epsB == 1) ;
#         correct = 1;
#         if  xor(k, ones(size(k)))
#             correct = 0;
#         end

#         if correct
#             % test for duplicate matches
#             distsMA = (f1(1,correctA) - f1(1,matches(1,i))).^2 + (f1(2,correctA) - f1(2,matches(1,i))).^2;
#             distsMB = (f2(1,correctB) - f2(1,matches(2,i))).^2 + (f2(2,correctB) - f2(2,matches(2,i))).^2;
#             epsMA = distsMA < 0.01^2;
#             epsMB= distsMB < 0.01^2;
            
#             nDuplicate = ~(epsMA == 1 | epsMB == 1);
#             if isempty(epsMA) | (nDuplicate)
#                 numCorrMtchs = numCorrMtchs +1;
#                 correctA = [correctA matches(1,i)];
#                 correctB = [correctB matches(2,i)];
#             end
#         end
#     end
#     correctMatches = [correctA; correctB];
# end


#     scores = {}
#     for dataset in D:

#         print('=> Starting benchmarking on  dataset ' + dataset +'.')
#         datasetPath = os.path.join(benchPath, dataset)
#         toMatch = loadmat(os.path.join(datasetPath, 'toMatch.mat'))['toMatch']

        
#         for match in toMatch:
#             iA = match[0]
#             iB = match[1]
#             imA = os.path.join(datasetPath, f'img{iA}.jpg')
#             imB = os.path.join(datasetPath, f'img{iB}.jpg')
#             kpA = os.path.join(datasetPath, f'ktps{iA}.mat')
#             kpB = os.path.join(datasetPath, f'ktps{iB}.mat')


#             pvxA = loadmat(kpA)['pvxA']
#             pvxB = loadmat(kpB)['pvxB']


#             imgA = cv2.cvtColor(cv2.imread(imA), cv2.COLOR_BGR2RGB)
#             imgB = cv2.cvtColor(cv2.imread(imB), cv2.COLOR_BGR2RGB)

#             sA = imgA.shape
#             sB = imgB.shape

#             #draw_matches_o(pvxA, pvxB, imgA, imgB)

#             dat = 'X:\CIIRC\BP\images\wGt\SPNet\\'

#             spk1 = loadmat(os.path.join(dat, f'img{iA}.jpg.mat'))['keypoints'][:,:2]
#             spk2 = loadmat(os.path.join(dat, f'img{iB}.jpg.mat'))['keypoints'][:,:2]
#             spkM = loadmat(os.path.join(dat, f'img{iA}.jpg-img{iB}.jpg.mat'))['matches'][:,:]

#             H, maskMag = pymagsac.findFundamentalMatrix(x1y1=spk1[spkM[:, 0], :], x2y2=spk2[spkM[:, 1],: ], w1=sA[1], h1=sA[0], w2=sB[1], h2=sB[0])

#             spkMM = spkM[maskMag, :]
#             draw_matches_o(spk1.T, spk2.T, imgA, imgB, spkM.T)
#             draw_matches_o(spk1.T, spk2.T, imgA, imgB, spkMM.T)

#             spkMMV = verifyMatches(spkMM.T, spk1.T, spk2.T, pvxA, pvxB, 7)
            
#             #iR = rn.randint(0, pvxA.shape[1], size=(1, 80)) if rand else np.arange(pvxA.shape[1])
#             draw_matches_o(spk1.T, spk2.T, imgA, imgB, spkMMV)
#             print(f'orig: {spkM.shape[0]} - V: {spkMMV.shape[1]} /  MS: {spkMM.shape[0]} = {spkMMV.shape[1]/ spkMM.shape[0]*100}%')
#             print('End.')
            


    #         pkTs = linspace(0,21,22);
    # %         egTs = linspace(2,22,21);
    # % 
    # %         I1 = single(rgb2gray(imgA));
    # %         I2 = single(rgb2gray(imgB));
    # %         EPSILON = 6;
    # % 
    # %         [srt_scr, idx] = sort(scores, 'ascend');
    # %         matches = matches(:,idx);
    # %         fprintf('=> Starting data extraction for\n   %s, \n   %s, \n   with epsilon of %d.\n', imA, imB, EPSILON);
    # % 
    # %         allData.imA = imA;
    # %         allData.imB = imB;
    # %         allData.kpA = kpA;
    # %         allData.kpB = kpB;
    # %         allData.sA = sA;
    # %         allData.sB = sB;
    # %         allData.eps = EPSILON;
    # %         allData.pkTs = pkTs;
    # %         allData.egTs = egTs;
    # % 
    # %         allIters = size(pkTs, 2)*size(egTs, 2);
    # %         currIter = 0;
    # %         for iPkT=1:size(pkTs, 2)
    # %             pkT = pkTs(iPkT);
    # %             for iEgT = 1:size(egTs, 2)
    # %                 egT = egTs(iEgT);
    # %                 currIter = currIter+1;
    # %                 fprintf(' > Iteration %d/%d with PE %d and ET %d\n', currIter, allIters, pkT, egT);
    # %                 feature extraction
    # %                 [f1,d1] = vl_sift(I1);%,'PeakThresh', pkT, 'edgethresh', egT);
    # %                 [f2,d2] = vl_sift(I2);%,'PeakThresh', pkT, 'edgethresh', egT);
    # %                 [matches, scores] = vl_ubcmatch(d1, d2, 1.5);
    # % 
    # %                 verMatches = verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON);
    # % 
    # %                 iterData.pT = pkT;
    # %                 iterData.eT = egT;
    # %                 iterData.nMa = size(matches, 2);
    # %                 iterData.nVMa = size(verMatches, 2);
    # %                 iterData.ma = matches;
    # %                 iterData.vMa = verMatches;
    # % 
    # %                 iterName = sprintf('iterP%dE%d', pkT, egT);
    # %                 allData.iters.(iterName) = iterData;
    # %                 figure();
    # %                 imshow(img12); hold on;
    # % 
    # %                 matches = verMatches;
    # %                 plot(f1(1,matches(1,:)), f1(2, matches(1,:)), 'b.', 'MarkerSize', 5)
    # %                 plot((f2(1,matches(2,:)) + sA(2)), f2(2,matches(2,:)), 'b.', 'MarkerSize', 5)
    # %                 for i = 1:numel(matches(1,:))
    # %                     plot([f1(1,matches(1,i)) (f2(1,matches(2,i)) + sA(2))], [f1(2, matches(1,i)) f2(2,matches(2,i))], 'g', 'LineWidth', 0.4);
    # %                 end
    # %             end
    # %         end
    #     end
    # end

    # save(outF, '-struct', 'allData');
    # fprintf('=> Data saved, process Finished.\n');


    # function correctMatches = verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON)
    #     numCorrMtchs = 0;
    #     correctA = [];
    #     correctB = [];
    #     for i=1:size(matches, 2)
    #         distsA = (pvxA(1,:)-f1(1,matches(1,i))).^2 + (pvxA(2,:)-f1(2,matches(1,i))).^2;
    #         distsB = (pvxB(1,:)-f2(1,matches(2,i))).^2 + (pvxB(2,:)-f2(2,matches(2,i))).^2;
    #         epsA = distsA < EPSILON^2;
    #         epsB = distsB < EPSILON^2;
            
    #         k = (epsA == 1 & epsB == 1) ;
    #         correct = 1;
    #         if  xor(k, ones(size(k)))
    #             correct = 0;
    #         end

    #         if correct
    #             % test for duplicate matches
    #             distsMA = (f1(1,correctA) - f1(1,matches(1,i))).^2 + (f1(2,correctA) - f1(2,matches(1,i))).^2;
    #             distsMB = (f2(1,correctB) - f2(1,matches(2,i))).^2 + (f2(2,correctB) - f2(2,matches(2,i))).^2;
    #             epsMA = distsMA < 0.01^2;
    #             epsMB= distsMB < 0.01^2;
                
    #             nDuplicate = ~(epsMA == 1 | epsMB == 1);
    #             if isempty(epsMA) | (nDuplicate)
    #                 numCorrMtchs = numCorrMtchs +1;
    #                 correctA = [correctA matches(1,i)];
    #                 correctB = [correctB matches(2,i)];
    #             end
    #         end
    #     end
    #     correctMatches = [correctA; correctB];
    # end

