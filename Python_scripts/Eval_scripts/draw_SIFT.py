
import cv2 as cv


imp = 'X:\\CIIRC\\datasets\\pipes\\'
imn = 'DSC_0634.JPG'
img = cv.imread(imp + imn)
gray= cv.cvtColor(img,cv.COLOR_BGR2GRAY)

# contrastThreshold = 0.04, edgeThreshold = 10
# nOctaveLayers = 3, sigma = 1.6 
sift = cv.SIFT_create()


kp = sift.detect(gray,None)
img=cv.drawKeypoints(gray,kp,img)
cv.imwrite(imn + 'sift_keypoints.jpg',img)

img=cv.drawKeypoints(img,kp,img,flags=cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
cv.imwrite(imn + 'sift_keypoints_rich.jpg',img)