from scipy.io import savemat, loadmat

def printec(inserter):
    print(inserter)

    
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Verify given matches using MAGSAC.')

    parser.add_argument('--out_path', type=str, required=True, help='Path to save verified matches.')

    args = parser.parse_args()
    savemat(args.out_path + "test.mat", {'a':123})