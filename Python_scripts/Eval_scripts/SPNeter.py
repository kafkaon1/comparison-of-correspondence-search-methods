import os
import cv2
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rn 
import pymagsac
from scipy.io import loadmat, savemat
import json


def verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON):
    ''' f1, f2 - kpts found by detector; matches - matches between f1 and f2 
        pvxA, pvxB - ground truth; EPSILON - acceptable match error '''
    
    numCorrMtchs = 0
    correctA = []
    correctB = []

    for i in range(matches.shape[1]):

        distsA = (pvxA[0,:]-f1[0,matches[0,i]])**2 + (pvxA[1,:]-f1[1,matches[0,i]])**2
        distsB = (pvxB[0,:]-f2[0,matches[1,i]])**2 + (pvxB[1,:]-f2[1,matches[1,i]])**2
        epsA = distsA < EPSILON**2
        epsB = distsB < EPSILON**2

        if np.logical_and(epsA, epsB).any():

            # test for duplicate matches
            distsMA = (f1[0,correctA] - f1[0,matches[0,i]])**2 + (f1[1,correctA] - f1[1,matches[0,i]])**2
            distsMB = (f2[0,correctB] - f2[0,matches[1,i]])**2 + (f2[1,correctB] - f2[1,matches[1,i]])**2
            epsMA = distsMA < 0.01**2
            epsMB= distsMB < 0.01**2
            
            dup = np.logical_and(epsMA, epsMB).any()

            if not dup:
                numCorrMtchs = numCorrMtchs + 1
                correctA = np.append(correctA, matches[0,i]).astype(np.uint32)
                correctB = np.append(correctB, matches[1,i]).astype(np.uint32)

    return np.stack((correctA, correctB)).astype(np.uint32)

def draw_matches_o(pvxA, pvxB, imgA, imgB, iR):
    imgAB = np.concatenate((imgA,imgB), axis=1)
    
    plt.axis('off')
    plt.imshow(imgAB)

    if iR.size == 0:
        return
   
    sA = imgA.shape
    
    plt.scatter(pvxA[0,iR[0,:]], pvxA[1, iR[0,:]], 0.5, c='blue', alpha=0.5)
    plt.scatter(pvxB[0, iR[1,:]] + sA[1], pvxB[1,iR[1,:]], 0.5, c='blue', alpha=0.5)
    plt.plot(np.stack((pvxA[0,iR[0,:]],pvxB[0,iR[1,:]]+sA[1])),
         np.stack((pvxA[1, iR[0,:]],pvxB[1,iR[1,:]])),
         c='limegreen',
         linewidth=0.7)
    plt.show()
    return

if __name__=='__main__':

    
    dataset_name = 'HoloLens_hall_up3'
    dataPath = 'X:\\CIIRC\\BP\\benchmark\\results\\' + dataset_name
    datasetPath = 'X:\\CIIRC\\BP\\benchmark\\' + dataset_name

    toMatch = loadmat(os.path.join(datasetPath, 'toMatch.mat'))['toMatch']
    
    crashed = True

    
    for match in toMatch:
        iA = match[0]
        iB = match[1]
        #matches
        # iA = toMatch[1,0]
        # iB = toMatch[1,1] 
        #load iters to verify
        allIters = loadmat(os.path.join(dataPath, f'datI_{iA}_{iB}_SP_r.mat'))['SPmatches']

        #set oupath
        outF = os.path.join(dataPath, f'datI_{iA}_{iB}_SP_MAG.mat')

        print(f'Matching {iA}-{iB} from {dataset_name}')
        
        #load image and GT
        imA = os.path.join(datasetPath, f'img{iA}.jpg')
        imB = os.path.join(datasetPath, f'img{iB}.jpg')
        kpA = os.path.join(datasetPath, f'ktps{iA}.mat')
        kpB = os.path.join(datasetPath, f'ktps{iB}.mat')
        
        imgA = cv2.cvtColor(cv2.imread(imA), cv2.COLOR_BGR2RGB)
        imgB = cv2.cvtColor(cv2.imread(imB), cv2.COLOR_BGR2RGB)
        pvxA = loadmat(kpA)['pvxA']
        pvxB = loadmat(kpB)['pvxB']

        sA = imgA.shape
        sB = imgB.shape

        mtchNums = range(2, 102, 2)
        numIs = len(mtchNums)

        kptsA = allIters[0][0].T
        kptsB = allIters[0][1].T
        matchesAll = allIters[0][2]
        numMatchesAll = matchesAll.shape[0]

        if not os.path.exists(outF):
            crashed = False
        if crashed == False:
            allItersUpdated = np.array([])
            il = 0
        else:
            allItersUpdated = loadmat(outF)['itersUpdated']
            il = allItersUpdated.shape[0]
            print(f'Lodaded last checkpoint of size:{il}')
        
        #test results for differnet number of matches taken
        for i in range(il, len(mtchNums)):
            print(f'Iteration {i} of {numIs}.')

            nMatches = np.int32(numMatchesAll*(mtchNums[i]/100))
            matches = matchesAll[:nMatches,:].T

            maskMG = []
            if matches.shape[1] > 7:
                H, maskMG = pymagsac.findFundamentalMatrix(x1y1=kptsA[:, matches[0, :]].T, x2y2=kptsB[:, matches[1, :]].T, w1=sA[1], h1=sA[0], w2=sB[1], h2=sB[0],max_iters=1800)#, use_magsac_plus_plus=False, sigma_th=1, partition_num=8, max_iters=1000, conf=0.99)
                print('Fundamental matrix found.')

            matchesMG = matches[:, maskMG]
            matchesMGV = verifyMatches(matchesMG, kptsA, kptsB, pvxA, pvxB, 6)
            # draw_matches_o(kptsA, kptsB, imgA, imgB, matches)
            # draw_matches_o(kptsA, kptsB, imgA, imgB, matchesMG)
            # draw_matches_o(kptsA, kptsB, imgA, imgB, matchesMGV)
            print('VM:' + str(matchesMGV.shape[1]))

            iterUpdated = np.reshape(np.array([mtchNums[i], matchesMG, matchesMGV], dtype=object), [1,3])
            
            if allItersUpdated.size ==0:
                allItersUpdated = iterUpdated
            else:
                allItersUpdated = np.concatenate((allItersUpdated, iterUpdated))

            if i%2 == 0:
                print("Saving")
                savemat(outF,{'itersUpdated':allItersUpdated})

        savemat(outF,{'itersUpdated':allItersUpdated})
        print('Iterations completed and saved.')




