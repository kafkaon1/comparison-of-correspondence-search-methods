#  BT codes, Ondrej Kafka

import numpy as np
import matplotlib.pyplot as plt
from scipy.io import savemat, loadmat
import cv2

from time import time
from copy import deepcopy

import pymagsac

def decolorize(img):
    return  cv2.cvtColor(cv2.cvtColor(img,cv2.COLOR_RGB2GRAY), cv2.COLOR_GRAY2RGB)

def draw_matches(kps1, kps2, tentatives, img1, img2, mask):
    matchesMask = mask.ravel().tolist()
    # Blue is estimated, green is ground truth homography
    draw_params = dict(matchColor = (255,255,0), # draw matches in yellow color
                   singlePointColor = None,
                   matchesMask = matchesMask, # draw only inliers
                   flags = 2)
    img_out = cv2.drawMatches(decolorize(img1),kps1,img2,kps2,tentatives,None,**draw_params)
    plt.figure(figsize = (12,8))
    plt.imshow(img_out)
    return

def draw_matches_o(kps1, kps2, tentatives, img1, img2, mask):
    pass

def verify_cv2_fundam(kps1, kps2, tentatives):
    src_pts = np.float32([ kps1[m.queryIdx].pt for m in tentatives ]).reshape(-1,1,2)
    dst_pts = np.float32([ kps2[m.trainIdx].pt for m in tentatives ]).reshape(-1,1,2)
    H, mask = cv2.findFundamentalMat(src_pts, dst_pts, cv2.RANSAC, 0.75)
    print (deepcopy(mask).astype(np.float32).sum(), 'inliers found')
    return H, mask

def verify_pymagsac_fundam(kps1, kps2, tentatives, use_magsac_plus_plus):
    src_pts = np.float32([ kps1[m.queryIdx].pt for m in tentatives ]).reshape(-1,2)
    dst_pts = np.float32([ kps2[m.trainIdx].pt for m in tentatives ]).reshape(-1,2)
    H, mask = pymagsac.findFundamentalMatrix(
        np.ascontiguousarray(src_pts),
        np.ascontiguousarray(dst_pts),
        use_magsac_plus_plus = use_magsac_plus_plus,
        sigma_th = 3.0)
    print (deepcopy(mask).astype(np.float32).sum(), 'inliers found')
    return H, mask

def adaptive_inlier_selection(kps1, kps2, tentatives, model, maximum_threshold, minimum_required_inliers):
    src_pts = np.float32([ kps1[m.queryIdx].pt for m in tentatives ]).reshape(-1,2)
    dst_pts = np.float32([ kps2[m.trainIdx].pt for m in tentatives ]).reshape(-1,2)

    mask, inlier_number, best_threshold = pymagsac.adaptiveInlierSelection(
        np.ascontiguousarray(src_pts),
        np.ascontiguousarray(dst_pts),
        np.ascontiguousarray(model),
        maximumThreshold = maximum_threshold, # An upper bound for the threshold, this speeds up the procedure, but it should work with infinity as well
        problemType = 1, # 1 stands for fundamental matrix fitting
        minimumInlierNumber = minimum_required_inliers)

    return mask, inlier_number, best_threshold

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Verify given matches using MAGSAC.')

    parser.add_argument('--kpA', type=str, default='X:\CIIRC\BP\images\wGt\SPNet\img1.jpg.mat', help='Path to .mat file with keypoints1.')
    parser.add_argument('--kpB', type=str, default='X:\CIIRC\BP\images\wGt\SPNet\img38.jpg.mat', help='Path to .mat file with keypoints1.')
    parser.add_argument('--matches', type=str, default='X:\CIIRC\BP\images\wGt\SPNet\img1.jpg-img38.jpg.mat', help='Path to .mat file with matches between kpts1 and kpts2.')
    
    parser.add_argument('--out_path', type=str, default='X:\CIIRC\BP\images\wGt\SPNet\\', help='Path to save verified matches.')
    #parser.add_argument('--keypointsA', type=str, required=True, help='Path to .mat file with keypoints1.')
    #parser.add_argument('--keypointsB', type=str, required=True, help='Path to .mat file with keypoints1.')
    #parser.add_argument('--matchez', type=str, required=True, help='Path to .mat file with matches between kpts1 and kpts2.')

    args = parser.parse_args()
    kpA = np.array(loadmat(args.kpA)['keypoints'], dtype=np.float64)
    kpB = np.array(loadmat(args.kpB)['keypoints'], dtype=np.float64)
    matches = np.array(loadmat(args.matches)['matches'], dtype=np.uint8)
    w = 1344.0
    h = 756.0
    H, mask = pymagsac.findFundamentalMatrix(x1y1=kpA[matches[0:20, 0], 0:2].tolist(), x2y2=kpB[matches[0:20, 1], 0:2].tolist(), w1=w, h1=h, w2=w, h2=h)


    savemat(args.out_path + "verified.mat", {'mask':mask})
    print('cao')
    #draw_matches(kps1, kps2, tentatives, img1, img2,np.arange(A.size))
    # t=time()
    # cv2_F, cv2_mask = verify_cv2_fundam(kps1,kps2,tentatives)
    # print (time()-t, 'sec cv2')

    # t=time()
    # magpp_F, magpp_mask = verify_pymagsac_fundam(kps1, kps2, tentatives, True)
    # print (time()-t, ' sec magsac++')

    # t=time()
    # mag_F, mag_mask = verify_pymagsac_fundam(kps1, kps2, tentatives, False)
    # print (time()-t, ' sec magsac')

    # draw_matches(kps1, kps2, tentatives, img1, img2, cv2_mask)
    # draw_matches(kps1, kps2, tentatives, img1, img2, mag_mask)
    # draw_matches(kps1, kps2, tentatives, img1, img2, magpp_mask)


    # mask, inlier_number, best_threshold = adaptive_inlier_selection(kps1, kps2, tentatives, magpp_F, 10.0, 20)
    # draw_matches(kps1, kps2, tentatives, img1, img2, mask)

    # print("""Note: the number of inliers does not determine the quality of the solution.
    #     This adaptive inlier selection algorithm selects the set of inliers such that they lead
    #     to the most similar model after LSQ fitting to the estimated one.
    #     This might be useful e.g. in SfM pipelines when using MAGSAC or MAGSAC++.
    #     If more inliers are needed, parameter 'minimum_required_inliers' should be set higher""")
    # print(inlier_number, 'inliers are found by the adaptive strategy')
    # print(best_threshold, 'px is the best threshold')