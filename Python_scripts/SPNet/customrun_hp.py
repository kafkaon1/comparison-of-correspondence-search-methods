import faiss
import torch
import torch.nn as nn
from torch.autograd import Variable

import os
from os.path import exists, join, basename
from collections import OrderedDict

import sys
sys.path.append('..')

from lib.model import ImMatchNet, MutualMatching
from lib.normalization import imreadth, resize, normalize
from lib.torch_util import str_to_bool
from lib.point_tnf import normalize_axis,unnormalize_axis,corr_to_matches
from lib.sparse import get_matches_both_dirs, torch_to_me, me_to_torch, unique
from lib.relocalize import relocalize, relocalize_soft, eval_model_reloc

import numpy as np
import numpy.random
from scipy.io import loadmat
from scipy.io import savemat
import pdb
import argparse
import time

import matplotlib.pyplot as plt


use_cuda = torch.cuda.is_available()

# Argument parsing
parser = argparse.ArgumentParser()
parser.add_argument('--checkpoint', type=str, default='./trained_models/sparsencnet_k10.pth.tar')
parser.add_argument('--hseq_path', type=str, default='./datasets/proj_onrec_1_default/images')
parser.add_argument('--k_size', type=int, default=1)
parser.add_argument('--image_size', type=int, default=1600)
parser.add_argument('--experiment_name', type=str, default='sparsencnet_3200_hard_soft')
parser.add_argument('--symmetric_mode', type=str_to_bool, default=True)
parser.add_argument('--nchunks', type=int, default=1)
parser.add_argument('--chunk_idx', type=int, default=0)
parser.add_argument('--skip_up_to', type=str, default='')
parser.add_argument('--relocalize', type=int, default=1)
parser.add_argument('--reloc_type', type=str, default='hard')
parser.add_argument('--reloc_hard_crop_size', type=int, default=2)
parser.add_argument('--change_stride', type=int, default=1)
parser.add_argument('--benchmark', type=int, default=0)
parser.add_argument('--no_ncnet', type=int, default=0)
parser.add_argument('--Npts', type=int, default=4000)

TABLE_C = 2

def idxFromHash(table, points, x, y):
    idx = table[int(TABLE_C*y + 0.5), int(TABLE_C*x + 0.5)]
    if idx == -1:
        idx = table.max() + 1
        table[int(TABLE_C*y + 0.5), int(TABLE_C*x + 0.5)] = idx
        points.append([x,y])
    return idx

def getPointIndices(table, points, A, NP):
    indices = np.zeros(NP, dtype=np.uint32)

    for i in range(NP):
        indices[i] = idxFromHash(table, points, A[i,0], A[i,1])
    return indices

if __name__ == "__main__":
    ''' EXTRACTION '''

    args = parser.parse_args()
    print(f'=== Running matching on{args.hseq_path}.')
    #print(args)
    mtchs = os.path.join(args.hseq_path,'matches')
    use_cuda = torch.cuda.is_available()
    if not os.path.exists(mtchs):
        os.mkdir(mtchs)

    chp_args = torch.load(args.checkpoint)['args']
    model = ImMatchNet(use_cuda=use_cuda,
                       checkpoint=args.checkpoint,
                       ncons_kernel_sizes=chp_args.ncons_kernel_sizes,
                       ncons_channels=chp_args.ncons_channels,
                       sparse=True,
                       symmetric_mode=bool(chp_args.symmetric_mode),
                       feature_extraction_cnn=chp_args.feature_extraction_cnn,
                       bn=bool(chp_args.bn),
                       k=chp_args.k,
                       return_fs=True,
                       change_stride=args.change_stride
                      )


    scale_factor = 0.0625
    if args.relocalize==1:
        scale_factor = scale_factor/2
    if args.change_stride==1:
        scale_factor = scale_factor*2

    # Get shortlists for each query image
    dataset_path=args.hseq_path
    seq_names = sorted(os.listdir(dataset_path))

    seq_names=np.array(seq_names)
    seq_names_split = np.array_split(seq_names,args.nchunks)
    seq_names_chunk = seq_names_split[args.chunk_idx]

    seq_names_chunk=list(seq_names_chunk)
    if args.skip_up_to!='':
        seq_names_chunk = seq_names_chunk[seq_names_chunk.index(args.skip_up_to)+1:]

    #pdb.set_trace()
    images = {}


    for i in range(len(seq_names_chunk)):
        src_name = seq_names_chunk[i]
        if src_name[-4:] != '.jpg':
            continue

        src_fn = os.path.join(args.hseq_path,src_name)
        srco=imreadth(src_fn)
        hA,wA=srco.shape[-2:]
        hashTable = np.ones([TABLE_C*hA,TABLE_C*wA])*-1
        refCount = np.zeros([TABLE_C*hA,TABLE_C*wA])
        keyPoints = []
        images[src_name] = {'hashTab': hashTable, 'count': refCount, 'keyPts': keyPoints}



    for i in range(len(seq_names_chunk)):
        src_name = seq_names_chunk[i]
        if src_name[-4:] != '.jpg':
            continue

        for j in range(i+1, len(seq_names_chunk)):

            tgt_name = seq_names_chunk[j]

            if tgt_name[-4:] != '.jpg':
                continue

            if src_name == tgt_name:
                continue

            src_fn = os.path.join(args.hseq_path,src_name)
            srco=imreadth(src_fn)
            hA,wA=srco.shape[-2:]
            src=resize(normalize(srco), args.image_size, scale_factor)
            hA_,wA_=src.shape[-2:]

            tgt_fn = os.path.join(args.hseq_path,tgt_name)
            tgto=imreadth(tgt_fn)
            hB,wB=tgto.shape[-2:]
            tgt=resize(normalize(tgto), args.image_size, scale_factor)
            hB_,wB_=tgt.shape[-2:]

            start = time.time()

            with torch.no_grad():
                if args.benchmark:
                    corr4d, feature_A_2x, feature_B_2x, fs1, fs2, fs3, fs4, fe_time, cnn_time = eval_model_reloc(
                        model,
                        {'source_image':src,
                         'target_image':tgt},
                        args
                    )
                else:
                    corr4d, feature_A_2x, feature_B_2x, fs1, fs2, fs3, fs4 = eval_model_reloc(
                        model,
                        {'source_image':src,
                         'target_image':tgt},
                        args
                    )

                delta4d=None


            xA_, yA_, xB_, yB_, score_ = get_matches_both_dirs(corr4d, fs1, fs2, fs3, fs4)

            if args.Npts is not None:
                matches_idx_sorted = torch.argsort(-score_.view(-1))
    #             if args.relocalize:
    #                 N_matches = min(int(args.Npts*1.25), matches_idx_sorted.shape[0])
    #             else:
    #                 N_matches = min(args.Npts, matches_idx_sorted.shape[0])
                N_matches = min(args.Npts, matches_idx_sorted.shape[0])
                matches_idx_sorted = matches_idx_sorted[:N_matches]
                score_ = score_[:,matches_idx_sorted]
                xA_ = xA_[:,matches_idx_sorted]
                yA_ = yA_[:,matches_idx_sorted]
                xB_ = xB_[:,matches_idx_sorted]
                yB_ = yB_[:,matches_idx_sorted]



            #pdb.set_trace()
            if args.relocalize:
                fs1,fs2,fs3,fs4=2*fs1,2*fs2,2*fs3,2*fs4
                # relocalization stage 1:
                if args.reloc_type.startswith('hard'):
                    xA_, yA_, xB_, yB_, score_ = relocalize(xA_,
                                                            yA_,
                                                            xB_,
                                                            yB_,
                                                            score_,
                                                            feature_A_2x,
                                                            feature_B_2x,
                                                            crop_size=args.reloc_hard_crop_size)
                    if args.reloc_hard_crop_size==3:
                        _,uidx = unique(yA_.double()*fs2*fs3*fs4+xA_.double()*fs3*fs4+yB_.double()*fs4+xB_.double(),return_index=True)
                        xA_=xA_[:,uidx]
                        yA_=yA_[:,uidx]
                        xB_=xB_[:,uidx]
                        yB_=yB_[:,uidx]
                        score_=score_[:,uidx]
                elif args.reloc_type=='soft':
                    xA_, yA_, xB_, yB_, score_ = relocalize_soft(xA_,yA_,xB_,yB_,score_,feature_A_2x, feature_B_2x)

                # relocalization stage 2:
                if args.reloc_type=='hard_soft':
                    xA_, yA_, xB_, yB_, score_ = relocalize_soft(xA_,yA_,xB_,yB_,score_,feature_A_2x, feature_B_2x, upsample_positions=False)

                elif args.reloc_type=='hard_hard':
                    xA_, yA_, xB_, yB_, score_ = relocalize(xA_,yA_,xB_,yB_,score_,feature_A_2x, feature_B_2x, upsample_positions=False)

            yA_=(yA_+0.5)/(fs1)
            xA_=(xA_+0.5)/(fs2)
            yB_=(yB_+0.5)/(fs3)
            xB_=(xB_+0.5)/(fs4)

            #pdb.set_trace()
            xA = xA_.view(-1).data.cpu().float().numpy()*wA
            yA = yA_.view(-1).data.cpu().float().numpy()*hA
            xB = xB_.view(-1).data.cpu().float().numpy()*wB
            yB = yB_.view(-1).data.cpu().float().numpy()*hB

            start1 = time.time()
            keypoints_A=np.stack((xA,yA),axis=1)
            keypoints_B=np.stack((xB,yB),axis=1)
            pointsA = images[src_name]['keyPts']
            pointsB = images[tgt_name]['keyPts']
            idx_A = getPointIndices(images[src_name]['hashTab'], pointsA, keypoints_A, 3000)
            idx_B = getPointIndices(images[tgt_name]['hashTab'], pointsB, keypoints_B, 3000)
            end1 = time.time()
            print(end1 - start1)
            #idx_A =np.arange(args.Npts//10, dtype=np.uint32)#.reshape(-1,1)#(yA_*fs2+xA_).view(-1,1)
            #idx_B =np.arange(args.Npts//10, dtype=np.uint32)#.reshape(-1,1) #(yB_*fs4+xB_).view(-1,1)

            matches = np.stack((idx_A, idx_B)).T
            #pdb.set_trace()
            score = score_.view(-1,1).cpu().numpy()

            src_tgt = (torch.cat((srco,tgto),dim=2))
            src_tgt = src_tgt.squeeze().permute(1,2,0).mul(255).cpu().numpy().astype(np.uint8)

            pA = np.array(pointsA)
            pB = np.array(pointsB)
            plt.imshow(src_tgt)
            plt.axis('off')
            plt.scatter(pA[:,0],pA[:,1],0.5,c='limegreen',alpha=0.5)
            plt.scatter(pB[:,0]+wA,pB[:,1],0.5,c='limegreen',alpha=0.5)
            plt.plot(np.stack((pA[matches[:,0],0],pB[matches[:,1],0]+wA)),
                     np.stack((pA[matches[:,0],1],pB[matches[:,1],1])),
                     c='limegreen',
                     linewidth=0.1)
            plt.gcf().set_dpi(600)

            plt.show()
            #plt.savefig(f'{src_name[:-4]}-{tgt_name[:-4]}.png')
            #plt.close()

            matches_file = 'matches/{}_and_{}_matched.npz.{}'.format(src_name,tgt_name,args.experiment_name)
            matches_fn = f'{src_name}-{tgt_name}.{args.experiment_name}'
            matches_path = os.path.join(args.hseq_path,'matches',matches_fn)

            end = time.time()
            print(f'Matched images {src_name} and {tgt_name}, w. elapsed_time {end-start}.')


            with open(matches_path, 'wb') as output_file:
                np.savez(output_file, matches=matches,
                                    keypoints=keypoints_A)

            #print(matches_file)
            #pdb.set_trace()
            del corr4d,delta4d,src,tgt, feature_A_2x, feature_B_2x
            del xA,xB,yA,yB,score
            del xA_,xB_,yA_,yB_,score_
            torch.cuda.empty_cache()
            torch.cuda.reset_max_memory_allocated()
    for i in range(len(seq_names_chunk)):
        src_name = seq_names_chunk[i]
        if src_name[-4:] != '.jpg':
            continue

        pdb.set_trace()
        kp_A_fn = os.path.join(args.hseq_path,'matches', src_name+'.'+args.experiment_name )
        with open(kp_A_fn, 'wb') as output_file:
            np.savez(output_file,keypoints=images[src_name]['keyPts'])
