import faiss
import torch
import torch.nn as nn
from torch.autograd import Variable

import os
from os.path import exists, join, basename
from collections import OrderedDict

import sys
sys.path.append('..')

from lib.model import ImMatchNet, MutualMatching
from lib.normalization import imreadth, resize, normalize
from lib.torch_util import str_to_bool
from lib.point_tnf import normalize_axis,unnormalize_axis,corr_to_matches
from lib.sparse import get_matches_both_dirs, torch_to_me, me_to_torch, unique
from lib.relocalize import relocalize, relocalize_soft, eval_model_reloc

import numpy as np
import numpy.random
from scipy.io import loadmat
from scipy.io import savemat
import argparse
import time

import matplotlib.pyplot as plt

from scipy.io import savemat

use_cuda = torch.cuda.is_available()

# Argument parsing
parser = argparse.ArgumentParser()
parser.add_argument('--checkpoint', type=str, default='./trained_models/sparsencnet_k10.pth.tar')
parser.add_argument('--iA', type=int, default=1)
parser.add_argument('--iB', type=int, default=38)
parser.add_argument('--inPath', type=str, default='/home/ondin/Dokumenty/CIIRC/benchmark/HoloLens_door')
parser.add_argument('--outPath', type=str, default='/home/ondin/Dokumenty/CIIRC/benchmark/results/HoloLens_door')
parser.add_argument('--experiment_name', type=str, default='spar_bp_bench')

parser.add_argument('--k_size', type=int, default=1)
parser.add_argument('--image_size', type=int, default=1344)
parser.add_argument('--symmetric_mode', type=str_to_bool, default=True)
parser.add_argument('--nchunks', type=int, default=1)
parser.add_argument('--chunk_idx', type=int, default=0)
parser.add_argument('--skip_up_to', type=str, default='')
parser.add_argument('--relocalize', type=int, default=1)
parser.add_argument('--reloc_type', type=str, default='hard_soft')
parser.add_argument('--reloc_hard_crop_size', type=int, default=2)
parser.add_argument('--change_stride', type=int, default=1)
parser.add_argument('--benchmark', type=int, default=0)
parser.add_argument('--no_ncnet', type=int, default=0)
parser.add_argument('--Npts', type=int, default=None)
#parser.add_argument('--Nmtchs', type=int, default=4000)

TABLE_C = 3
SCORE_TSH = 590

def unifyMatches(matches):
    #each match needs to be unique
    uniValsA = set()
    uniValsB = set()
    uniMatches = []
    for i in range(len(matches)):
        if not (matches[i,0] in uniValsA or matches[i,1] in uniValsB):
            uniValsA.add(matches[i,0])
            uniValsB.add(matches[i,1])
            uniMatches.append(matches[i].tolist())

    return np.array(uniMatches, dtype=np.int32)


def registerImgs(image, keyPts, scores):
    lastI = image['tMax']
    table = image['hashTab']
    ptAr = image['keyPts']

    indices = []
    maskSize = len(ptAr)
    mask = np.zeros([maskSize, 4], dtype=np.uint32)

    for i in range(len(keyPts)):
        p = keyPts[i]
        x_r = int(TABLE_C*p[1] + 0.5) #rounded x_coord
        y_r = int(TABLE_C*p[0] + 0.5)
        idx = table[x_r, y_r]
    
        if idx == -1:
            lastI += 1
            idx = lastI 
            table[x_r, y_r] = idx
            ptAr = np.concatenate((ptAr,[[p[0], p[1], 1, scores[i]]]))
        elif idx < maskSize:
            mask[int(idx)][2] = 1
            mask[int(idx)][3] = max(mask[int(idx)][3], scores[i])

        indices.append(idx)
    
    image['tMax'] = lastI
    ptAr[0:maskSize] += mask
    image['keyPts'] = ptAr

    return np.array(indices, dtype=np.int32)

def softmax(x):
    ex = np.exp(x)
    sm = np.sum(ex)
    return ex/sm

if __name__ == "__main__":
    ''' EXTRACTION '''

    args = parser.parse_args()
    print(f'=== Running matching on{args.inPath}, {args.iA}-{args.iB}.')

    mtchs = args.outPath
    
    use_cuda = torch.cuda.is_available()
    if not os.path.exists(mtchs):
        os.mkdir(mtchs)

    chp_args = torch.load(args.checkpoint)['args']
    model = ImMatchNet(use_cuda=use_cuda,
                       checkpoint=args.checkpoint,
                       ncons_kernel_sizes=chp_args.ncons_kernel_sizes,
                       ncons_channels=chp_args.ncons_channels,
                       sparse=True,
                       symmetric_mode=bool(chp_args.symmetric_mode),
                       feature_extraction_cnn=chp_args.feature_extraction_cnn,
                       bn=bool(chp_args.bn),
                       k=chp_args.k,
                       return_fs=True,
                       change_stride=args.change_stride
                      )


    scale_factor = 0.0625
    if args.relocalize==1:
        scale_factor = scale_factor/2
    if args.change_stride==1:
        scale_factor = scale_factor*2

    src_name = f'img{args.iA}.jpg'
    tgt_name = f'img{args.iB}.jpg'
    outFN =os.path.join(args.outPath, f'datI_{args.iA}_{args.iB}_SP_r.mat')

    seq_names_chunk = [src_name, tgt_name]
    images = {}

    for i in range(len(seq_names_chunk)):
        img_name = seq_names_chunk[i]
        if img_name[-4:] != '.jpg':
            continue

        img_fn = os.path.join(args.inPath,img_name)
        imgo=imreadth(img_fn)
        hA,wA=imgo.shape[-2:]
        hashTable = np.ones([TABLE_C*hA,TABLE_C*wA], dtype=np.int32)*-1
        keyPoints = np.zeros([0,4])
        matches = {}
        tMax = -1   #max index in the hastable
        images[img_name] = {'hashTab': hashTable, 'keyPts': keyPoints, 'mtchs': matches, 'tMax': tMax}

            
    start = time.time()

    src_fn = os.path.join(args.inPath,src_name)
    srco=imreadth(src_fn)
    hA,wA=srco.shape[-2:]
    src=resize(normalize(srco), args.image_size, scale_factor)
    hA_,wA_=src.shape[-2:]

    tgt_fn = os.path.join(args.inPath,tgt_name)
    tgto=imreadth(tgt_fn)
    hB,wB=tgto.shape[-2:]
    tgt=resize(normalize(tgto), args.image_size, scale_factor)
    hB_,wB_=tgt.shape[-2:]

    start1 = time.time()
    with torch.no_grad():
        if args.benchmark:
            corr4d, feature_A_2x, feature_B_2x, fs1, fs2, fs3, fs4, fe_time, cnn_time = eval_model_reloc(
                model,
                {'source_image':src,
                    'target_image':tgt},
                args
            )
        else:
            corr4d, feature_A_2x, feature_B_2x, fs1, fs2, fs3, fs4 = eval_model_reloc(
                model,
                {'source_image':src,
                    'target_image':tgt},
                args
            )

        delta4d=None


    xA_, yA_, xB_, yB_, score_ = get_matches_both_dirs(corr4d, fs1, fs2, fs3, fs4)

    if args.Npts is not None:
        matches_idx_sorted = torch.argsort(-score_.view(-1))
        N_matches = min(args.Npts, matches_idx_sorted.shape[0])
        matches_idx_sorted = matches_idx_sorted[:N_matches]
        score_ = score_[:,matches_idx_sorted]
        xA_ = xA_[:,matches_idx_sorted]
        yA_ = yA_[:,matches_idx_sorted]
        xB_ = xB_[:,matches_idx_sorted]
        yB_ = yB_[:,matches_idx_sorted]

    if args.relocalize:
        fs1,fs2,fs3,fs4=2*fs1,2*fs2,2*fs3,2*fs4
        # relocalization stage 1:
        if args.reloc_type.startswith('hard'):
            xA_, yA_, xB_, yB_, score_ = relocalize(xA_,
                                                    yA_,
                                                    xB_,
                                                    yB_,
                                                    score_,
                                                    feature_A_2x,
                                                    feature_B_2x,
                                                    crop_size=args.reloc_hard_crop_size)
            if args.reloc_hard_crop_size==3:
                _,uidx = unique(yA_.double()*fs2*fs3*fs4+xA_.double()*fs3*fs4+yB_.double()*fs4+xB_.double(),return_index=True)
                xA_=xA_[:,uidx]
                yA_=yA_[:,uidx]
                xB_=xB_[:,uidx]
                yB_=yB_[:,uidx]
                score_=score_[:,uidx]
        elif args.reloc_type=='soft':
            xA_, yA_, xB_, yB_, score_ = relocalize_soft(xA_,yA_,xB_,yB_,score_,feature_A_2x, feature_B_2x)

        # relocalization stage 2:
        if args.reloc_type=='hard_soft':
            xA_, yA_, xB_, yB_, score_ = relocalize_soft(xA_,yA_,xB_,yB_,score_,feature_A_2x, feature_B_2x, upsample_positions=False)

        elif args.reloc_type=='hard_hard':
            xA_, yA_, xB_, yB_, score_ = relocalize(xA_,yA_,xB_,yB_,score_,feature_A_2x, feature_B_2x, upsample_positions=False)

    yA_=(yA_+0.5)/(fs1)
    xA_=(xA_+0.5)/(fs2)
    yB_=(yB_+0.5)/(fs3)
    xB_=(xB_+0.5)/(fs4)

    xA = xA_.view(-1).data.cpu().float().numpy()*wA
    yA = yA_.view(-1).data.cpu().float().numpy()*hA
    xB = xB_.view(-1).data.cpu().float().numpy()*wB
    yB = yB_.view(-1).data.cpu().float().numpy()*hB

    end1 = time.time()
    print(f'FF time:{end1-start1}.')

    keypoints_A=np.stack((xA,yA),axis=1) #matched points in first image
    keypoints_B=np.stack((xB,yB),axis=1)

    score = score_.view(-1).cpu().numpy().astype(np.int32)

    #register keypoints
    idx_A = registerImgs(images[src_name], keypoints_A, score)
    idx_B = registerImgs(images[tgt_name], keypoints_B, score)

    #idx_A =np.arange(args.Npts//10, dtype=np.uint32)#.reshape(-1,1)#(yA_*fs2+xA_).view(-1,1)
    #idx_B =np.arange(args.Npts//10, dtype=np.uint32)#.reshape(-1,1) #(yB_*fs4+xB_).view(-1,1)

    references = np.zeros(idx_A.shape, dtype=np.int32)
    matchez = np.stack((idx_A, idx_B, score, references)).T
    images[src_name]['mtchs'][tgt_name] =  matchez

    #Visualisation
    # matches = np.stack((idx_A, idx_B)).T[:125,:]
    # src_tgt = (torch.cat((srco,tgto),dim=2))
    # src_tgt = src_tgt.squeeze().permute(1,2,0).mul(255).cpu().numpy().astype(np.uint8)
    
    # pA = np.array(images[src_name]['keyPts'][:,0:2])
    # pB = np.array(images[tgt_name]['keyPts'][:,0:2])
    # plt.imshow(src_tgt)
    # plt.axis('off')
    # plt.scatter(pA[:,0],pA[:,1],0.5,c='limegreen',alpha=0.5)
    # plt.scatter(pB[:,0]+wA,pB[:,1],0.5,c='limegreen',alpha=0.5)
    # plt.plot(np.stack((pA[matches[:,0],0],pB[matches[:,1],0]+wA)),
    #             np.stack((pA[matches[:,0],1],pB[matches[:,1],1])),
    #             c='limegreen',
    #             linewidth=0.1)
    # plt.gcf().set_dpi(600)

    # plt.show()
    #plt.savefig(f'{src_name[:-4]}-{tgt_name[:-4]}.png')
    #plt.close()

    end = time.time()
    print(f'Matched images {src_name} and {tgt_name}, w. elapsed_time {end-start:.2f}s.')

    #print(matches_file)

    del corr4d,delta4d,src,tgt, feature_A_2x, feature_B_2x
    del xA,xB,yA,yB,score
    del xA_,xB_,yA_,yB_,score_
    torch.cuda.empty_cache()
    torch.cuda.reset_max_memory_allocated()

    defMtchs = unifyMatches(images[src_name]['mtchs'][tgt_name])
    ml = 0 #len of matches array
    
    keyPtsA = images[src_name]['keyPts']    # contains coords of registered points fo
    keyPtsB = images[tgt_name]['keyPts']

    found_low = 0
    for i in range(len(defMtchs)):

        #third column = sum of referred scores, 
        #secon = num of references
        refA =  keyPtsA[defMtchs[i,0],3]
        refB =  keyPtsB[defMtchs[i,1],3]
        defMtchs[i,3] = refA + refB


    
    ixs = np.lexsort((-defMtchs[:,2], -defMtchs[:,3]))
    ixs = np.argsort(-defMtchs[:,3])
    matSor = defMtchs[ixs]
    
    matches = matSor[:, 0:2]



    toSave = np.reshape(np.array([keyPtsA[:,0:2], keyPtsB[:,0:2], matches], dtype=object), [1,3])

    savemat(outFN, {'SPmatches':toSave})

    # matches = matches[:125, 0:2]
    # src_tgt = (torch.cat((srco,tgto),dim=2))
    # src_tgt = src_tgt.squeeze().permute(1,2,0).mul(255).cpu().numpy().astype(np.uint8)
    
    
    # pA = keyPtsA[:,0:2]
    # pB = keyPtsB[:,0:2]

    # plt.imshow(src_tgt)
    # plt.axis('off')
    # alphaA = keyPtsA[:,3]/ keyPtsA[:,3].max()
    # alphaB = keyPtsB[:,3]/ keyPtsB[:,3].max()
    # colorsA = []
    # colorsB = []

    # for i in range(len(alphaA)):
    #     colorsA.append(((1-alphaA[i]), alphaA[i], 0.5))
    # for i in range(len(alphaB)):
    #     colorsB.append(((1-alphaB[i]), alphaB[i], 0.5))

    # plt.scatter(pA[:,0],pA[:,1],0.9,c=colorsA)
    # plt.scatter(pB[:,0]+wA,pB[:,1],0.9,c=colorsB)
    # plt.plot(np.stack((pA[matches[:,0],0],pB[matches[:,1],0]+wA)),
    #             np.stack((pA[matches[:,0],1],pB[matches[:,1],1])),
    #             c='blue',
    #             linewidth=0.1)

    # # plt.gcf().set_dpi(600)

    # plt.show()
    #print('fin')