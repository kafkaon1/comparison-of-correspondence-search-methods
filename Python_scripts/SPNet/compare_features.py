import numpy as np
import numpy.random as rn
import time
import torch.nn as nn
import torch
'''
A =  np.load('./datasets/proj_onrec_1_default/images/matches/ap1.jpg-ap5.jpg.sparsencnet_3200_hard_soft')['keypoints']
A2 = np.load('./datasets/proj_onrec_1_default/images/matches/ap1.jpg-ap7.jpg.sparsencnet_3200_hard_soft')['keypoints']

n = 0
counts = np.zeros(A.shape[0])
counts2 = np.zeros(A.shape[0])
counts3 = np.zeros(A.shape[0])

for i in range(A.shape[0]):
    if A[i,:] in A2:
        counts2[i]+=1
        if t:
            counts3[i]+=1
print(n)
#a1 = dict(zip(np.unique(counts, return_counts=True)))
a, b = np.unique(counts2, return_counts=True)
#a3 = dict(zip(np.unique(counts3, return_counts=True)))
pdb.set_trace()

'''

Npts = 40
TABLE_C = 2
hA =    20
wA = 30

images = {}

src_name = 'ap1'
hashTable = np.ones([TABLE_C*hA,TABLE_C*wA])*-1
keyPoints = np.zeros([0,4])
matches = {}
tMax = -1   # max index in the hastable
images[src_name] = {'hashTab': hashTable, 'keyPts': keyPoints, 'mtchs': matches, 'tMax': tMax}

def softmax(x):
    ex = np.exp(x)
    sm = np.sum(ex)
    return ex/sm


def unifyMatches(matches):
    uniValsA = set()
    uniValsB = set()
    uniMatches = []
    for i in range(len(matches)):
        if not (matches[i,0] in uniValsA or matches[i,1] in uniValsB):
            uniValsA.add(matches[i,0])
            uniValsB.add(matches[i,1])
            uniMatches.append(matches[i].tolist())

    return np.array(uniMatches, dtype=np.int32)


def registerImgs(image, keyPts, scores):
    lastI = image['tMax']
    table = image['hashTab']
    ptAr = image['keyPts']

    indices = []
    maskSize = len(ptAr)
    mask = np.zeros([maskSize, 4], dtype=np.uint32)
    times = []

    for i in range(len(keyPts)):
        p = keyPts[i]
        start = time.time()
        x_r = int(TABLE_C*p[1] + 0.5) #rounded x_coord
        y_r = int(TABLE_C*p[0] + 0.5)
        idx = table[x_r, y_r]
    
        if idx == -1:
            lastI += 1
            idx = lastI 
            table[x_r, y_r] = idx
            ptAr = np.concatenate((ptAr,[[p[0], p[1], 1, scores[i]]]))
        elif idx < maskSize:
            mask[int(idx)][2] = 1
            mask[int(idx)][3] = max(mask[int(idx)][3], scores[i])

        indices.append(idx)
        end = time.time()
        times.append(end-start)
    
    image['tMax'] = lastI
    ptAr[0:maskSize] += mask
    image['keyPts'] = ptAr

    return np.array(indices, dtype=np.int32)


xs = rn.randint(wA, size=(Npts))
ys = rn.randint(hA, size=(Npts))
keypoints = np.stack((xs, ys), axis=1)

xs2 = rn.randint(wA, size=(Npts))
ys2 = rn.randint(hA, size=(Npts))
keypoints2 = np.stack((xs2, ys2), axis=1)

scores = rn.randint(400, size=(Npts))
scores = scores[np.argsort(-scores)]

scores2 = rn.randint(400, size=(Npts))
scores2 = scores2[np.argsort(-scores2)]

#start1 = time.time()
#ix =  getPointIndices(hashTable, keyPoints, keypoints, Npts)
inds = registerImgs(images[src_name], keypoints, scores)
inds2 = registerImgs(images[src_name], keypoints2, scores2)


#mat = unifyMatches(keypoints)

print('end')
#end1 = time.time()
#print(end1 - start1)