
import subprocess
import os

DTSTS = ['./datasets/proj_onrec_1_default', './datasets/proj_onrec_2_sensitive',
    './datasets/proj_onrec_3_def_3photos','./datasets/proj_onrec_3_sens_3photos']

for dt in DTSTS:
    subprocess.call(['python3', './customrun_hp.py',
                     '--hseq_path', os.path.join(dt,'images')])
    subprocess.call(['python3', './customrec_hp.py', '--dataset_path', dt])
