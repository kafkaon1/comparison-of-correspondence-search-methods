
import subprocess
import os
from scipy.io import loadmat, savemat



DTST = '/home/ondin/Dokumenty/CIIRC/benchmark'
RES = '/home/ondin/Dokumenty/CIIRC/benchmark/results'

# for dt in DTSTS:
#     subprocess.call(['python3', './customrun_hp.py',
#                      '--hseq_path', os.path.join(dt,'images')])
#     subprocess.call(['python3', './customrec_hp.py', '--dataset_path', dt])


datasets = os.listdir(DTST)[1:]

for dataset in datasets:
    currDS = os.path.join(DTST, dataset)
    results_dir = os.path.join(RES, dataset)

    toMatch = loadmat(os.path.join(currDS, 'toMatch.mat'))['toMatch']
    for match in toMatch:
        iA = match[0]
        iB = match[1]


        print(f'>>> Running SPNet on{currDS}, {iA}-{iB}.')

        subprocess.call(['python3', './runSP.py',
                     '--iA', str(iA), '--iB', str(iB),
                     '--outPath', results_dir,
                     '--inPath', currDS])
