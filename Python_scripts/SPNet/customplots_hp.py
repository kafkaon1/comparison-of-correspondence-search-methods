import matplotlib

import matplotlib.pyplot as plt

import numpy as np

import os

import torch

from scipy.io import loadmat

from tqdm import tqdm_notebook as tqdm

# https://www.somersault1824.com/wp-content/uploads/2015/02/color-blindness-palette-e1423327633855.png
cbcolors={1:(0,0,0),
          2:(0,73,73),
          3:(0,146,146),
          4:(255,109,182),
          5:(255,182,119),
          6:(73,0,146),
          7:(0,109,219),
          8:(182,109,255),
          9:(109,182,255),
          10:(182,219,255),
          11:(146,0,0),
          12:(146,73,0),
          13:(219,209,0),
          14:(36,255,36),
          15:(255,255,109)}
cbcolors={k:(v[0]/255,v[1]/255,v[2]/255) for k,v in cbcolors.items()}
