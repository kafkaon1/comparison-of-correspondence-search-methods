import trimesh
import pyrender
import numpy as np

tm = trimesh.load('examples/models/fuze.obj')
m = pyrender.Mesh.from_trimesh(tm)