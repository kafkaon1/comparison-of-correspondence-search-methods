#! /usr/bin/env python3

import argparse
import random
from pathlib import Path
from scipy.io import loadmat, savemat

import matplotlib.cm as cm
import numpy as np
import torch

from models.matching import Matching
from models.utils import (AverageTimer, compute_epipolar_error,
                          compute_pose_error, error_colormap, estimate_pose,
                          make_matching_plot, pose_auc, read_image,
                          rotate_intrinsics, rotate_pose_inplane,
                          scale_intrinsics)

torch.set_grad_enabled(False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Image pair matching and pose evaluation with SuperGlue',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        '--iA', type=int, default=1,
        help='index of image A')
    parser.add_argument(
        '--iB', type=int, default=38,
        help='index of image B')
    parser.add_argument(
        '--input_dir', type=str, default='/home/ondin/Dokumenty/CIIRC/benchmark/HoloLens_door',
        help='Path to the directory that contains the images')
    parser.add_argument(
        '--output_dir', type=str, default='/home/ondin/Dokumenty/CIIRC/benchmark/results/HoloLens_door/SG7',
        help='Path to the directory in which the .npz results and optionally,'
             'the visualization images are written')

    parser.add_argument(
        '--max_length', type=int, default=-1,
        help='Maximum number of pairs to evaluate')
    parser.add_argument(
        '--resize', type=int, nargs='+', default=[640,480],
        help='Resize the input image before running inference. If two numbers, '
             'resize to the exact dimensions, if one number, resize the max '
             'dimension, if -1, do not resize')

    parser.add_argument(
        '--resize_float', action='store_true',
        help='Resize the image after casting uint8 to float')

    parser.add_argument(
        '--superglue', choices={'indoor', 'outdoor'}, default='indoor',
        help='SuperGlue weights')
    parser.add_argument(
        '--max_keypoints', type=int, default=-1,
        help='Maximum number of keypoints detected by Superpoint'
             ' (\'-1\' keeps all keypoints)')
    parser.add_argument(
        '--keypoint_threshold', type=float, default=0.00003,
        help='SuperPoint keypoint detector confidence threshold')
    parser.add_argument(
        '--nms_radius', type=int, default=4,
        help='SuperPoint Non Maximum Suppression (NMS) radius'
        ' (Must be positive)')
    parser.add_argument(
        '--sinkhorn_iterations', type=int, default=20,
        help='Number of Sinkhorn iterations performed by SuperGlue')
    parser.add_argument(
        '--match_threshold', type=float, default=0.06,
        help='SuperGlue match threshold')

    parser.add_argument(
        '--viz', type=bool, default=False,
        help='Visualize the matches and dump the plots')
    parser.add_argument(
        '--fast_viz', action='store_true',
        help='Use faster image visualization with OpenCV instead of Matplotlib')
    parser.add_argument(
        '--cache', action='store_true',
        help='Skip the pair if output .npz files are already found')
    parser.add_argument(
        '--show_keypoints', action='store_true',
        help='Plot the keypoints in addition to the matches')
    parser.add_argument(
        '--viz_extension', type=str, default='png', choices=['png', 'pdf'],
        help='Visualization file extension. Use pdf for highest-quality.')
    parser.add_argument(
        '--opencv_display', action='store_true',
        help='Visualize via OpenCV before saving output images')
    parser.add_argument(
        '--force_cpu', action='store_true',
        help='Force pytorch to run in CPU mode.')

    opt = parser.parse_args()
    print(opt)

    assert not (opt.opencv_display and not opt.viz), 'Must use --viz with --opencv_display'
    assert not (opt.opencv_display and not opt.fast_viz), 'Cannot use --opencv_display without --fast_viz'
    assert not (opt.fast_viz and not opt.viz), 'Must use --viz with --fast_viz'
    assert not (opt.fast_viz and opt.viz_extension == 'pdf'), 'Cannot use pdf extension with --fast_viz'

    print(f'  => Running matching on{opt.input_dir}, {opt.iA}-{opt.iB}.')

    if len(opt.resize) == 2 and opt.resize[1] == -1:
        opt.resize = opt.resize[0:1]
    if len(opt.resize) == 2:
        print('Will resize to {}x{} (WxH)'.format(
            opt.resize[0], opt.resize[1]))
    elif len(opt.resize) == 1 and opt.resize[0] > 0:
        print('Will resize max dimension to {}'.format(opt.resize[0]))
    elif len(opt.resize) == 1:
        print('Will not resize images')
    else:
        raise ValueError('Cannot specify more than two integers for --resize')

    
    imA_na = f'img{opt.iA}.jpg'
    imB_na = f'img{opt.iB}.jpg'
    pairs = [[imA_na, imB_na]]

    # Load the SuperPoint and SuperGlue models.
    device = 'cuda' if torch.cuda.is_available() and not opt.force_cpu else 'cpu'
    print('Running inference on device \"{}\"'.format(device))

    sp_ths = np.array([0, 0.00001, 0.00003, 0.00006, 0.0001, 0.0003, 0.0006, 0.001, 0.003, 0.006,0.01, 0.03, 0.06, 0.1, 0.3, 0.6, 1], dtype=np.float32)

    sg_ths =  np.array([0, 0.001, 0.003, 0.006,0.01, 0.03, 0.06, 0.1, 0.3, 0.6, 1], dtype=np.float32)

    allIters = len(sp_ths)*len(sg_ths)
    currIte= 0
    allItersUpdated = np.array([])


    for sp_th in sp_ths:
        for sg_th in sg_ths:
            currIte +=1

            print(f'  SG: Iteration {currIte}/{allIters}.')
            config = {
                'superpoint': {
                    'nms_radius': opt.nms_radius,
                    'keypoint_threshold': sp_th,
                    'max_keypoints': opt.max_keypoints
                },
                'superglue': {
                    'weights': opt.superglue,
                    'sinkhorn_iterations': opt.sinkhorn_iterations,
                    'match_threshold': sg_th
                }
            }
            matching = Matching(config).eval().to(device)

            # Create the output directories if they do not exist already.
            input_dir = Path(opt.input_dir)
            #print('Looking for data in directory \"{}\"'.format(input_dir))
            output_dir = Path(opt.output_dir)
            output_dir.mkdir(exist_ok=True, parents=True)
            outFN = output_dir / f'datI_{opt.iA}_{opt.iB}_SG_r.mat'
            
            '''
            #print('Will write matches to directory \"{}\"'.format(output_dir))
            if opt.viz:
                print('Will write visualization images to',
                    'directory \"{}\"'.format(output_dir)) '''

            timer = AverageTimer(newline=True)
            for i, pair in enumerate(pairs):
                name0, name1 = pair[:2]
                stem0, stem1 = Path(name0).stem, Path(name1).stem
                matches_path = output_dir / '{}_{}_matches.npz'.format(stem0, stem1)
                viz_path = output_dir / '{}_{}_matches_{}_{}.{}'.format(stem0, stem1, sg_th, sp_th, opt.viz_extension)

                # Handle --cache logic.
                do_match = True
                do_viz = opt.viz
                if opt.cache:
                    if matches_path.exists():
                        try:
                            results = np.load(matches_path)
                        except:
                            raise IOError('Cannot load matches .npz file: %s' %
                                        matches_path)

                        kpts0, kpts1 = results['keypoints0'], results['keypoints1']
                        matches, conf = results['matches'], results['match_confidence']
                        do_match = False
                    if opt.viz and viz_path.exists():
                        do_viz = False
                    timer.update('load_cache')

                if not (do_match or do_viz):
                    timer.print('Finished pair {:5} of {:5}'.format(i, len(pairs)))
                    continue

                # If a rotation integer is provided (e.g. from EXIF data), use it:
                if len(pair) >= 5:
                    rot0, rot1 = int(pair[2]), int(pair[3])
                else:
                    rot0, rot1 = 0, 0

                # Load the image pair.
                image0, inp0, scales0 = read_image(
                    input_dir / name0, device, opt.resize, rot0, opt.resize_float)
                image1, inp1, scales1 = read_image(
                    input_dir / name1, device, opt.resize, rot1, opt.resize_float)
                if image0 is None or image1 is None:
                    print('Problem reading image pair: {} {}'.format(
                        input_dir/name0, input_dir/name1))
                    exit(1)
                timer.update('load_image')

                if do_match:
                    # Perform the matching.
                    pred = matching({'image0': inp0, 'image1': inp1})
                    pred = {k: v[0].cpu().numpy() for k, v in pred.items()}
                    kpts0, kpts1 = pred['keypoints0'], pred['keypoints1']
                    matches, conf = pred['matches0'], pred['matching_scores0']
                    timer.update('matcher')

                    # Write the matches to disk.
                    #out_matches = {'keypoints0': kpts0, 'keypoints1': kpts1,
                    #            'matches': matches, 'match_confidence': conf}
                    #np.savez(str(matches_path), **out_matches)

                # Keep the matching keypoints.
                valid = matches > -1
                # mkpts0 = kpts0[valid]
                # mkpts1 = kpts1[matches[valid]]
                # mconf = conf[valid]

                matchesA = np.arange(matches.shape[0])
                matchesA = matchesA[valid]
                matchesB = matches[valid]
                matRes = np.stack((matchesA, matchesB))

                mkpts0 = kpts0[matRes[0,:]]
                mkpts1 = kpts1[matRes[1,:]]
                mconf = conf[valid]

                if do_viz:
                    # Visualize the matches.
                    color = cm.jet(mconf)
                    text = [
                        'SuperGlue',
                        'Keypoints: {}:{}'.format(len(kpts0), len(kpts1)),
                        'Matches: {}'.format(len(mkpts0)),
                    ]
                    if rot0 != 0 or rot1 != 0:
                        text.append('Rotation: {}:{}'.format(rot0, rot1))

                    # Display extra parameter info.
                    k_thresh = matching.superpoint.config['keypoint_threshold']
                    m_thresh = matching.superglue.config['match_threshold']
                    small_text = [
                        'Keypoint Threshold: {:.4f}'.format(k_thresh),
                        'Match Threshold: {:.2f}'.format(m_thresh),
                        'Image Pair: {}:{}'.format(stem0, stem1),
                    ]

                    make_matching_plot(
                        image0, image1, kpts0, kpts1, mkpts0, mkpts1, color,
                        text, viz_path, opt.show_keypoints,
                        opt.fast_viz, opt.opencv_display, 'Matches', small_text)

                    timer.update('viz_match')

            iterUpdated = np.reshape(np.array([sp_th, sg_th, kpts0, kpts1, matRes], dtype=object), [1,5])
        
            if allItersUpdated.size == 0:
                allItersUpdated = iterUpdated
            else:
                allItersUpdated = np.concatenate((allItersUpdated, iterUpdated))

    
    print("SG iterating finished, saving.")
    savemat(outFN,{'itersSG':allItersUpdated})

        

    