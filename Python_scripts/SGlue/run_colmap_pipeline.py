import numpy as np
import os
import shutil
import subprocess
import sqlite3
import types
import collections
from scipy.io import loadmat, savemat
import matplotlib.pyplot as plt
import cv2
from database import COLMAPDatabase
from read_write_model import read_cameras_text, read_images_text

def draw_matches_o(pvxA, pvxB, imA, imB, iR):

    imgA = cv2.cvtColor(cv2.imread(imA), cv2.COLOR_BGR2RGB)
    imgB = cv2.cvtColor(cv2.imread(imB), cv2.COLOR_BGR2RGB)

    sA = imgA.shape
    sB = imgB.shape

    imgAB = np.concatenate((imgA,imgB), axis=1)
    
    plt.axis('off')
    plt.imshow(imgAB)

    if iR.size == 0:
        return
   
    sA = imgA.shape
    
    plt.scatter(pvxA[0,iR[0,:]], pvxA[1, iR[0,:]], 0.5, c='blue', alpha=0.5)
    plt.scatter(pvxB[0, iR[1,:]] + sA[1], pvxB[1,iR[1,:]], 0.5, c='blue', alpha=0.5)
    plt.plot(np.stack((pvxA[0,iR[0,:]],pvxB[0,iR[1,:]]+sA[1])),
         np.stack((pvxA[1, iR[0,:]],pvxB[1,iR[1,:]])),
         c='limegreen',
         linewidth=0.7)
    plt.show()
    return

def set_up_database(paths):
    #create_db
    if os.path.exists(paths.database_path):
        os.remove(paths.database_path)
        print("Old database removed!")
    subprocess.call([paths.colmap_executable_path, 'database_creator',
                        '--database_path', paths.database_path])
    
    #connect
    db = COLMAPDatabase.connect(paths.database_path)

    images = {}
    #extract cameras
    cameras = read_cameras_text(os.path.join(paths.calibration_path, 'cameras.txt'))
    for i in range(len(cameras)):
        cam = cameras[i]
        db.add_camera(1, cam.width, cam.height, cam.params, camera_id=cam.id)

    #extract images
    images = read_images_text(os.path.join(paths.calibration_path, 'images.txt'))
    for i in range(1, len(images)+1):
        im = images[i]
        #db.add_image(im.name.split("/")[-1], im.camera_id, prior_q=im.qvec, prior_t=im.tvec,image_id=im.id)
        im_name = im.name.split("/")[-1]
        db.add_image(im_name, im.camera_id,image_id=im.id)
        images[im.id] = im_name

    db.commit()
    db.close()
    return images



def import_features(images, paths):
    # Import the features.
    print('  Importing features.')

    # Connect to the database.
    db = COLMAPDatabase.connect(paths.database_path)

    for image_id, image_name in images.items():
        kpPath = os.path.join(paths.res_path, f'{image_name}.mat')
        keypoints = loadmat(kpPath)['kp']


        db.add_keypoints(image_id, keypoints.T)

    db.commit()
    db.close()


def create_pairs(images):
    # Get shortlists for each query image

    ks = list(images.keys())[::-1]

    image_pairs = []
    for i in range(len(ks)):
        iA = ks[i]

        for j in range(i+1, len(ks)):
            iB  = ks[j]
            image_pairs.append( (iA, iB) )

    return image_pairs


def import_matches(images, paths):
    # Match the features and insert the matches in the database.
    print('Matching...')

    # Connect to the database.
    db = COLMAPDatabase.connect(paths.database_path)

    image_pairs = create_pairs(images)

    for pair in image_pairs:
        iA, iB = pair
        imA = images[iA]
        imB = images[iB]

        matches = loadmat(os.path.join(paths.res_path, f'{imA}-{imB}_M.mat'))['matches']
        F = loadmat(os.path.join(paths.res_path, f'{imA}-{imB}_F.mat'))['F']

        # kpPathA = os.path.join(paths.res_path, f'{imA}.mat')
        # keypointsA = loadmat(kpPathA)['kp']
        # kpPathB = os.path.join(paths.res_path, f'{imB}.mat')
        # keypointsB = loadmat(kpPathB)['kp']
        # draw_matches_o(keypointsA, keypointsB, os.path.join(paths.image_path, imA), os.path.join(paths.image_path, imB), matches)
        
        db.add_matches(iA, iB, matches.T)
        db.add_two_view_geometry(iA, iB, matches.T, F=F, E=np.zeros([3,3]), H=np.zeros([3,3]), config=3)

    db.commit()
    db.close()

def reconstruct(paths):

    # subprocess.call([paths.colmap_executable_path, 'exhaustive_matcher',
    #                 '--database_path', paths.database_path])

    if not os.path.exists(paths.sparse_path):
        os.makedirs(paths.sparse_path)
    subprocess.call([paths.colmap_executable_path, 'mapper',
                    '--database_path', paths.database_path,
                    '--image_path', paths.image_path,
                    '--output_path', paths.sparse_path,
                    '--Mapper.min_model_size', '3',
                    '--Mapper.min_num_matches','10'])
                    # '--Mapper.abs_pose_min_num_inliers', '15',
                    # '--Mapper.init_max_error', '6',
                    # '--Mapper.init_max_forward_motion', '1',     
                    # '--Mapper.init_max_reg_trials', '10',
                    # '--Mapper.abs_pose_min_num_inliers', '15'])
                    # '--Mapper.abs_pose_min_inlier_ratio', '0.1'])

                    #                     '--Mapper.init_image_id1', '1',
                    # '--Mapper.init_image_id2', '2',

                    

    if not os.path.exists(paths.dense_path):
        os.makedirs(paths.dense_path)
    subprocess.call([paths.colmap_executable_path, 'image_undistorter',
                    '--image_path', paths.image_path,
                    '--input_path', os.path.join(paths.sparse_path, '0'),
                    '--output_path', paths.dense_path, 
                    '--output_type', 'COLMAP',
                    '--max_image_size', '2000' ])

    subprocess.call([paths.colmap_executable_path, 'patch_match_stereo',
                        '--workspace_path' , paths.dense_path,
                        '--workspace_format', 'COLMAP',
                        '--PatchMatchStereo.geom_consistency', 'true'])

    subprocess.call([paths.colmap_executable_path, 'stereo_fusion',
                        '--workspace_path', paths.dense_path,
                        '--workspace_format', 'COLMAP',
                        '--input_type', 'geometric',
                        '--output_path', os.path.join(paths.dense_path, 'fused.ply')])

    subprocess.call([paths.colmap_executable_path, 'poisson_mesher',
                        '--input_path', os.path.join(paths.dense_path, 'fused.ply'),
                        '--output_path', os.path.join(paths.dense_path, 'meshed-poisson.ply')])
    
    subprocess.call([paths.colmap_executable_path, 'delaunay_mesher',
                        '--input_path', paths.dense_path,
                        '--output_path', os.path.join(paths.dense_path, 'meshed-delaunay.ply')])

def evaluate_rec(paths):

    subprocess.call([ paths.eth3d_executable_path, 
                        '--reconstruction_ply_path', os.path.join(paths.dense_path, 'meshed-delaunay.ply'),
                        '--ground_truth_mlp_path', os.path.join(paths.ground_truth_path, 'scan_alignment.mlp'),
                        '--tolerances', '0.01,0.02,0.05,0.1,0.2,0.5'])

if __name__ == "__main__":
    ethRootPath = 'X:\\CIIRC\\BP\\ETH3D\\'
    resRootPath = 'X:\\CIIRC\\BP\\ETH3D\\results'
    colPath = 'X:\\CIIRC\\Colmap_orig\\build\\src\\exe\\Release'
    eth3DPath = 'X:\\CIIRC\\src\\ETH3DMultiViewEvaluation'

    methods = ['SIM', 'WSIM', 'SG', 'SGM', 'SP']
    crashed = True
    D = os.listdir(ethRootPath)[:-1]
    for dataset_name in D:
        for method in methods[:1]:
            ethPath = os.path.join(ethRootPath, dataset_name)
            resPath = os.path.join(resRootPath, dataset_name, method)

            # Create paths
            paths = types.SimpleNamespace()
            paths.res_path = resPath
            paths.database_path = os.path.join(resPath, method + '.db')
            paths.image_path = os.path.join(ethPath, 'images')
            paths.ground_truth_path =  os.path.join(ethPath, 'eval')
            paths.calibration_path = os.path.join(ethPath, 'calibration')
            paths.colmap_executable_path = os.path.join(colPath, 'colmap')
            paths.eth3d_executable_path = os.path.join(eth3DPath, 'ETH3DMultiViewEvaluation')
            paths.sparse_path = os.path.join(resPath,  'sparse')
            paths.dense_path = os.path.join(resPath, 'dense')
            
            # Reconstruction pipeline.
            images = set_up_database(paths)
            import_features(images, paths)
            import_matches(images, paths)
            reconstruct(paths)

            # Evaluate
            #evaluate_rec(paths)
