import os
import cv2
from scipy.io import loadmat, savemat
import numpy as np
import matplotlib.pyplot as plt

def draw_matches_o(pvxA, pvxB, imgA, imgB, iR):
    imgAB = np.concatenate((imgA,imgB), axis=1)
    
    plt.axis('off')
    plt.imshow(imgAB)

    if iR.size == 0:
        return
   
    sA = imgA.shape
    
    plt.scatter(pvxA[0,iR[0,:]], pvxA[1, iR[0,:]], 0.5, c='blue', alpha=0.5)
    plt.scatter(pvxB[0, iR[1,:]] + sA[1], pvxB[1,iR[1,:]], 0.5, c='blue', alpha=0.5)
    plt.plot(np.stack((pvxA[0,iR[0,:]],pvxB[0,iR[1,:]]+sA[1])),
         np.stack((pvxA[1, iR[0,:]],pvxB[1,iR[1,:]])),
         c='limegreen',
         linewidth=0.7)
    plt.show()
    return

kptscnt =np.zeros([1,1163])#np.zeros([1,51546])
if __name__ == "__main__":
    ethRootPath = 'X:\\CIIRC\\datasets\\'#'X:\\CIIRC\\BP\\ETH3D\\'
    resRootPath = 'X:\\CIIRC\\datasets\\results\\'#'X:\\CIIRC\\BP\\ETH3D\\results'
    colPath = 'X:\\CIIRC\\Colmap_orig\\build\\src\\exe\\Release'

    D = ['ap2']#os.listdir(ethRootPath)[1:2]
    for dataset_name in D:
        ethPath = os.path.join(ethRootPath, dataset_name)
        resDesPath = os.path.join(resRootPath, dataset_name)

        methods = os.listdir(resDesPath)
        for method in ['SI']: #methods:#[35:39]:
            if method.endswith('M'):
                continue
            print(f"+++++ Resaving for {method}.")

            resPath = os.path.join(resDesPath, method)
            imgsPath = os.path.join(ethPath, 'images')
            images = os.listdir(imgsPath)
            for i in range(len(images)):
                imA = images[i]
                kptsA = loadmat(os.path.join(resPath, f'{imA}.mat'))['kp']#*SCALE

                for j in range(i+1, len(images)):
                    imB = images[j]
                    
                    outM = os.path.join(resPath, f'{imA}-{imB}_M.mat')
                    print(f'Operating with {imA}-{imB}.')
                    if os.path.exists(outM):
                        print(f'File {outM} already exists.')
                        continue
                    kptsB = loadmat(os.path.join(resPath, f'{imB}.mat'))['kp']


                    matches = loadmat(os.path.join(resPath, f'{imA}-{imB}.mat'))['matches'] #*SCALE
                    if method.startswith('SP') and not method.startswith('SPA'):
                        matches = matches[:,:np.int16(matches.shape[1]*0.4)]

                    if method.startswith('SI') or method.startswith('WSI'):
                        matches = matches -1

                    # draw_matches_o(kptsA, kptsB, imgA, imgB, matches)

                    savemat(outM,{'matches':matches})

                
            


        

