import os
import cv2
from scipy.io import loadmat, savemat
import pymagsac
import numpy as np
import matplotlib.pyplot as plt

def draw_matches_o(pvxA, pvxB, imgA, imgB, iR):
    imgAB = np.concatenate((imgA,imgB), axis=1)
    
    plt.axis('off')
    plt.imshow(imgAB)

    if iR.size == 0:
        return
   
    sA = imgA.shape
    
    plt.scatter(pvxA[0,iR[0,:]], pvxA[1, iR[0,:]], 0.5, c='blue', alpha=0.5)
    plt.scatter(pvxB[0, iR[1,:]] + sA[1], pvxB[1,iR[1,:]], 0.5, c='blue', alpha=0.5)
    plt.plot(np.stack((pvxA[0,iR[0,:]],pvxB[0,iR[1,:]]+sA[1])),
         np.stack((pvxA[1, iR[0,:]],pvxB[1,iR[1,:]])),
         c='limegreen',
         linewidth=0.7)
    plt.show()
    return

kptscnt =np.zeros([1,62184])#np.zeros([1,51546])

if __name__ == "__main__":
    ethRootPath = 'X:\\CIIRC\\BP\\ETH3D\\'
    resRootPath = 'X:\\CIIRC\\BP\\ETH3D\\results'
    colPath = 'X:\\CIIRC\\Colmap_orig\\build\\src\\exe\\Release'

    #method = 'SP1M'
    crashed = True
    D = os.listdir(ethRootPath)[0:1]
    for dataset_name in D:
        ethPath = os.path.join(ethRootPath, dataset_name)
        resDesPath = os.path.join(resRootPath, dataset_name)

        methods = os.listdir(resDesPath)
        for method in methods[7:]:#['SPA13M']:#methods:
            if not method.endswith('M'):
                continue
            print(f"+++++ MAGSACKING for {method}.")
            resPath = os.path.join(resDesPath, method)
            imgsPath = os.path.join(ethPath, 'images')
            images = os.listdir(imgsPath)
            for i in range(len(images)):
                imA = images[i]
                kptsA = loadmat(os.path.join(resPath, f'{imA}.mat'))['kp']#*SCALE

                for j in range(i+1, len(images)):
                    imB = images[j]
                    
                    outM = os.path.join(resPath, f'{imA}-{imB}_M.mat')
                    outF = os.path.join(resPath, f'{imA}-{imB}_F.mat')
                    print(f'Operating with {imA}-{imB}.')
                    if os.path.exists(outM) and os.path.exists(outF):
                        print(f'File {outM} already exists.')
                        continue
                    kptsB = loadmat(os.path.join(resPath, f'{imB}.mat'))['kp']


                    matches = loadmat(os.path.join(resPath, f'{imA}-{imB}.mat'))['matches'] #*SCALE
                    if method.startswith('SP') and not method.startswith('SPA'):
                        matches = matches[:,:np.int16(matches.shape[1]*0.4)]
                    # if matches.any():
                    #     kptscnt[:,matches[0,:]] = kptscnt[:,matches[0,:]] +1
                    if method.startswith('SI') or method.startswith('WSI'):
                        matches = matches -1


                    imAn = os.path.join(ethPath, 'images', imA)
                    imBn = os.path.join(ethPath, 'images', imB)
                    imgA = cv2.cvtColor(cv2.imread(imAn), cv2.COLOR_BGR2RGB)
                    imgB = cv2.cvtColor(cv2.imread(imBn), cv2.COLOR_BGR2RGB)

                    sA = imgA.shape
                    sB = imgB.shape

                    maskMG = []
                    F = np.zeros([3,3])
                    if matches.shape[1] > 7:
                        F, maskMG = pymagsac.findFundamentalMatrix(x1y1=kptsA[:, matches[0, :]].T, x2y2=kptsB[:, matches[1, :]].T, w1=sA[1], h1=sA[0], w2=sB[1], h2=sB[0], max_iters=1500, sigma_th=3)#, use_magsac_plus_plus=False, sigma_th=1, partition_num=8, max_iters=1000, conf=0.99)
                        #H, maskMGH = pymagsac.findHomography(x1y1=kptsA[:, matches[0, :]].T, x2y2=kptsB[:, matches[1, :]].T, w1=sA[1], h1=sA[0], w2=sB[1], h2=sB[0])
                        print('  Fundamental matrix found.')
                    matchesMG = matches[:, maskMG]
                    #matchesMGH = matches[:, maskMGH]
                    # draw_matches_o(kptsA, kptsB, imgA, imgB, matches)
                    # draw_matches_o(kptsA, kptsB, imgA, imgB, matchesMG)
                    print('VM:' + str(matchesMG.shape[1]))
                    savemat(outM,{'matches':matchesMG})
                    if F is None:
                        F=np.zeros([3,3])
                    savemat(outF,{'F':F})
                    
            


        

