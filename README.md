# Comparison of correspondence search methods

Codes used in Ondrej Kafka's Bachelor thesis named Comparison of Methods for Matching of Images with Weakly Textured Areas.

## Description of files and usage

Files are diveded by types, I used MATLAB .m scripts and Python .py scripts.

* **Pyhon** - In order to use python scripts, user needs to download an install the following:
    * [SuperGlue](https://github.com/magicleap/SuperGluePretrainedNetwork)
    * [SparseNCNet](https://github.com/ignacio-rocco/sparse-ncnet)
    * [MAGSAC++]( https://github.com/danini/magsac)
    * [COLMAP](https://colmap.github.io/)

Then use the SPNet and SGLue scripts from the root folders of SuperGlue and Sparsencnet root folders respecitvely. In order to use the COLMAP pipleline, colmap executable path ad colmap executable path to the _colmap_pipeline.py_ script. MAGSAC++ needs to be compiled and added to python environment.

* **MATLAB** - To run the MATLAB scripts, following files are needed to be downloaded:
    * [CBM3D denoising filter](http://www.cs.tut.fi/~foi/GCF-BM3D/)
    * [ETH3D multi-view stereo benchmark](https://www.eth3d.net/documentation)
    
Then add paths to ETH3D executable and CBM3D functions to the scripts, which need them.
