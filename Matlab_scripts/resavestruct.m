function resavestruct(struct, path)

    allData = struct;

    a = [];

    names = fieldnames(allData.iters);
    
    for i=1:numel(names)
        a = [a; allData.iters.(names{i})];
    end

    save(path, 'a')
end