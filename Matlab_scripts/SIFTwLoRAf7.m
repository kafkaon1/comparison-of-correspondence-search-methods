clc; clear all;

dataset_name = 'HoloLens_hall_down';
datpath = fullfile('X:\\CIIRC\\BP\\benchmark\\', dataset_name);
respath = fullfile('X:\\CIIRC\\BP\\benchmark\\results\\', dataset_name);

toMatch = load(fullfile(datpath, 'toMatch.mat')).toMatch;

for i = 1:size(toMatch, 1)
    iA = toMatch(i,1);
    iB = toMatch(i,2);

    imA = fullfile(datpath, sprintf('img%d.jpg',iA));
    imB = fullfile(datpath, sprintf('img%d.jpg',iB));
    kpA = fullfile(datpath, sprintf('ktps%d.mat',iA));
    kpB = fullfile(datpath, sprintf('ktps%d.mat',iB));

    if ~exist(respath, 'file')
        mkdir(respath)
    end
    outF = fullfile(respath, sprintf('datI_%d_%d_LoRA.mat', iA, iB));
    
    if exist(outF, 'file')
        fprintf('%s already exists, skippig.\n', outF);
    	continue
    end
    load(kpA); load(kpB);

    imgA = imread(imA);
    imgB = imread(imB);

    sA = size(imgA);
    sB = size(imgB);
    img12 = [imgA imgB];

    addpath('X:\CIIRC\BP\solveryM\relative_pose_solvers');

%     figure()
%     imshow(imgA); hold on;
%     plot(pvxA(1,:), pvxA(2, :), 'b.', 'MarkerSize', 3)

%     iR = randi([0 size(pvxA,2)],1,30);

    % figure();
    % imshow(img12); hold on;
    % plot(pvxA(1,iR), pvxA(2, iR), 'b.', 'MarkerSize', 5)
    % plot((pvxB(1, iR) + sA(2)), pvxB(2,iR), 'b.', 'MarkerSize', 5)
    % for i = 1:numel(iR)
    %     plot([pvxA(1,iR(i)) (pvxB(1, iR(i)) + sA(2))], [pvxA(2, iR(i)) pvxB(2,iR(i))], 'g', 'LineWidth', 0.3);
    % end

    pkTs = [0, 0.025, 0.05, 0.075, 0.1, 0.125, 0.15, 0.175, 0.2, 0.225, 0.25, 0.275, 0.3, 0.325, 0.35, 0.375, 0.4, 0.425, 0.45, 0.475, 0.5,0.525, 0.55, 0.575, 0.6, 0.65, 0.7, 0.85, 1, 1.5, 2, 3, 5, 7, 10];%linspace(0,14,60);
    egTs = linspace(2,30,29);

    I1 = single(rgb2gray(imgA));
    I2 = single(rgb2gray(imgB));
    EPSILON = 6;

    % [srt_scr, idx] = sort(scores, 'ascend');
    % matches = matches(:,idx);
    fprintf('=> Starting data extraction for\n   %s, \n   %s.\n', imA, imB);

    allData.imA = imA;
    allData.imB = imB;
    allData.kpA = kpA;
    allData.kpB = kpB;
    allData.sA = sA;
    allData.sB = sB;
    allData.eps = EPSILON;
    allData.pkTs = pkTs;
    allData.egTs = egTs;

    allIters = size(pkTs, 2)*size(egTs, 2);
    currIter = 0;
    for iPkT=1:size(pkTs, 2)
        pkT = pkTs(iPkT);
        for iEgT = 1:size(egTs, 2)
            egT = egTs(iEgT);
            currIter = currIter+1;
            fprintf(' > Iteration %d/%d with PE %d and ET %d\n', currIter, allIters, pkT, egT);

            %feature extraction
            [f1,d1] = vl_sift(I1, 'PeakThresh', pkT, 'edgethresh', egT);
            [f2,d2] = vl_sift(I2, 'PeakThresh', pkT, 'edgethresh', egT);
            [matches, scores] = vl_ubcmatch(d1, d2, 1.5);
            
            %verMats = verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON);

            correspondences = [f1(1:2, matches(1,:)); f2(1:2, matches(2,:))]';
            gVerMatches = [];
            if size(matches,2) >= 7
                [score7pc, gVerMatches, model7pc] = vanillaFundamentalMatrixRANSAC(correspondences, 0.98, 1, 0);
                %gVerMatches = gVerMatches(1,:);%verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON);
            end


            gMatches = matches(:,gVerMatches);

            verMatches = verifyMatches(gMatches, f1, f2, pvxA, pvxB, EPSILON);
            
%             % show inliers 
%             figure(); 
%             imshow(img12); hold on;
%             correspondences = correspondences';
%             plot(correspondences(1,gVerMatches), correspondences(2,gVerMatches), 'gx', 'MarkerSize', 4, 'LineWidth', 1.1)
%             plot(correspondences(3, gVerMatches) + sA(2), correspondences(4, gVerMatches), 'gx', 'MarkerSize', 4, 'LineWidth', 1.1)
%             for i = 1:length(gVerMatches)
%                 if isVerified(gMatches(:,i), verMatches)
%                     color = 'g';
%                 else
%                     color = 'r';
%                 end
%                 plot([correspondences(1, gVerMatches(i)) (correspondences(3, gVerMatches(i)) + sA(2))], [correspondences(2, gVerMatches(i)) correspondences(4, gVerMatches(i))], color, 'LineWidth', 1.2)
%             end

            iterData.pT = pkT;
            iterData.eT = egT;
            iterData.kp1 = f1(1:2,:);
            iterData.kp2 = f2(1:2,:);
            iterData.ma = matches;
            iterData.vMa = verMatches;
            iterData.gMa = gMatches;

            iterName = sprintf('iterP%dE%d', int16(pkT*100), egT);
            allData.iters.(iterName) = iterData;

    %         figure();
    %         imshow(img12); hold on;
    %         
    %         matches = gMatches; %verMatches;
    %         plot(f1(1,matches(1,:)), f1(2, matches(1,:)), 'b.', 'MarkerSize', 5)
    %         plot((f2(1,matches(2,:)) + sA(2)), f2(2,matches(2,:)), 'b.', 'MarkerSize', 5)
    %         for i = 1:numel(matches(1,:))
    %             plot([f1(1,matches(1,i)) (f2(1,matches(2,i)) + sA(2))], [f1(2, matches(1,i)) f2(2,matches(2,i))], 'g', 'LineWidth', 0.4);
    %         end
        end
    end

    save(outF, '-struct', 'allData');
    resavestruct(allData, fullfile(respath, sprintf('datI_%d_%d_LoRA_r.mat', iA, iB)));
    fprintf('=> Data saved, process Finished.\n');
end

function correctMatches = verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON)
    numCorrMtchs = 0;
    correctA = [];
    correctB = [];
    for i=1:size(matches, 2)
        distsA = (pvxA(1,:)-f1(1,matches(1,i))).^2 + (pvxA(2,:)-f1(2,matches(1,i))).^2;
        distsB = (pvxB(1,:)-f2(1,matches(2,i))).^2 + (pvxB(2,:)-f2(2,matches(2,i))).^2;
        epsA = distsA < EPSILON^2;
        epsB = distsB < EPSILON^2;
        
        k = (epsA == 1 & epsB == 1) ;
        correct = 1;
        if  xor(k, ones(size(k)))
            correct = 0;
        end

        if correct
            % test for duplicate matches
            distsMA = (f1(1,correctA) - f1(1,matches(1,i))).^2 + (f1(2,correctA) - f1(2,matches(1,i))).^2;
            distsMB = (f2(1,correctB) - f2(1,matches(2,i))).^2 + (f2(2,correctB) - f2(2,matches(2,i))).^2;
            epsMA = distsMA < 0.01^2;
            epsMB= distsMB < 0.01^2;
            
            nDuplicate = ~(epsMA == 1 | epsMB == 1);
            if isempty(epsMA) | (nDuplicate)
                numCorrMtchs = numCorrMtchs +1;
                correctA = [correctA matches(1,i)];
                correctB = [correctB matches(2,i)];
            end
        end
    end
    correctMatches = [correctA; correctB];
end 


function isV = isVerified(match, mV)
    [C, ~, ~] = intersect(cast(match, 'int64')',cast(mV, 'int64')' ,'rows');
    if C
        isV = 1;
    else
        isV = 0;
    end
end