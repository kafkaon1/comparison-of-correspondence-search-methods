
clc; clear all;
benchdir = 'X:\CIIRC\BP\benchmark';
D = dir(benchdir);
D = D(~ismember({D.name}, {'.', '..', 'results'}));

fprintf("=> Starting average benchmarking for CLAHE.\n");

id = 2;
sumVans = 0;

bin_x = [0.001, 0.003, 0.006, 0.01, 0.03, 0.06, 0.1, 0.3, 0.6, 0.99];
bin_y = [4 8 12 16 20 24 32 36 40 44 48 52 56];

xBounds = [0,0.99];
yBounds = [4, 56];


sumHist = zeros([size(bin_x, 2),size(bin_y, 2)]);
nVans = struct();
hists = struct();
nu = 0;



for i = 1:numel(D)
    file = D(i);
    fprintf(" > Starting dataset %s.\n", file.name);
    toMatch = load(fullfile(file.folder, file.name, 'toMatch.mat')).toMatch ;
    
    for j = 1:size(toMatch,1)
        if j> 2
            continue
        end
        iA = toMatch(j,1);
        iB = toMatch(j,2);
        dists = ["rayleigh"; "uniform"; "exponential"];
        
        respath = fullfile('X:\CIIRC\BP\benchmark\results\', file.name);
        claMet =  fullfile(respath, sprintf('datI_%d_%d_Cla_met.mat',iA, iB));
        claMetMag =  fullfile(respath, sprintf('datI_%d_%d_Cla_met_MAG.mat',iA, iB));

        datsVan = load(claMetMag).meta;
        nVan = size(datsVan{3},2);
        nVanm = size(datsVan{2},2);
        nV = int16(round(nVan/nVanm*100));
        
        dist = dists(id, :);
        
        fprintf("  >Matching %d-th combination : %d-%d, with %s dist. \n", j, iA, iB, dist);
        claIters =  fullfile(respath, sprintf('datI_%d_%d_Cla_%s_r.mat', iA, iB, dist));
        claMag = fullfile(respath, sprintf('datI_%d_%d_Cla_%s_MAG.mat', iA, iB, dist));

        datsIt = load(claIters).iters;
        datsM = load(claMag).itersUpdated;

        if size(datsIt, 1) == size(datsM, 1)
            numIters = size(datsIt, 1);
        else
            error('Invalid input sizes')
        end

        [histDataMagVer, histDataMagVerRel] = createHist(datsIt, datsM, numIters, bin_x, bin_y);
        itername = sprintf('%s%d%s', file.name, j, dist);

        nVans.(itername) = nVan;
        sumVans = sumVans + nVan;
        hists.(itername) = histDataMagVer;
        sumHist = sumHist + histDataMagVer;
        nu = nu+1;

        
    end
end

% visualisation

iternames = fieldnames(hists);

outperformers = zeros(size(sumHist));
avgH = sumHist/nu;
avgVan = sumVans/nu;

for i=1:numel(iternames)
    nV = nVans.(iternames{i});
    sumH = hists.(iternames{i});
    outperformers = outperformers + (sumH > nV);
end


% Plot frequency values with surf
[x,y] = meshgrid(bin_x,bin_y);

d = (avgH-avgVan)';
p = d > 0;
d(p) = d(p) + 20;
n = d < 0;
d(n) = -10;
figure();
hold on;

for i = 1 : size(bin_x, 2)
    for j = 1: size(bin_y, 2)
        pA = bin_x(i);
        pB = bin_y(j);
        if (pA >= xBounds(1)) && (pA < xBounds(2)) && (pB >= yBounds(1)) && (pB < yBounds(2))
            txt = sprintf('%d',uint16((avgH(i,j)/avgVan-1)*100));
            if d(j, i) > 0
                text(pA+pA/10,pB + 0.05,txt, 'FontSize', 11.5);
            elseif d(j,i)< 0
                text(pA+pA/10,pB + 2,'-', 'FontSize', 15, 'FontWeight', 'bold');
            else
                text(pA+pA/10,pB + 2,'0', 'FontSize', 15, 'FontWeight', 'bold');
            end
         end
    end
end
   


surf(x,y,d);
view(2)
zlabel('Difference of number of matches');
title(sprintf('Average diff. of matches, CLAHE, %s',dist), 'Fontweight','Bold');
xlabel('Clip limit');
ylabel('Win size');
s = findobj(gca,'Type','Surface');
s.FaceAlpha = 0.5;
xlim(xBounds);
ylim(yBounds);
%colorbar;
grid on;
set(gca,'FontSize',15);
set(gca,'Linewidth',1.1);
set(gcf,'Position', [100,100,750,600]);
set(gca, 'XScale', 'log');


figure();
hold on;



for i = 1 : size(bin_x, 2)
    for j = 1: size(bin_y, 2)
        pA = bin_x(i);
        pB = bin_y(j);
        if (pA >= xBounds(1)) && (pA < xBounds(2)) && (pB >= yBounds(1)) && (pB < yBounds(2))
            txt = sprintf('%d', uint16(outperformers(i,j)./nu *100));
            text(pA+pA/10,pB + 2,txt, 'FontSize', 15, 'FontWeight', 'bold');
        end

    end
end
   


surf(x,y,outperformers');
view(2)
zlabel('Difference of number of matches');
title(sprintf('Perc. of outperformers, CLAHE, %s', dist), 'Fontweight','Bold')
xlabel('Clip limit');
ylabel('Window size');
s = findobj(gca,'Type','Surface');
s.FaceAlpha = 0.5;
%colorbar;
grid on;
xlim(xBounds);
ylim(yBounds);
set(gca,'FontSize',15);
set(gca,'Linewidth',1.1);
set(gcf,'Position', [100,100,750,600]);
set(gca, 'XScale', 'log');

   

function [cnt, cntRel] = createHist(datsIt, datsM, numIters, bin_x, bin_y)

    histDataMagVer = [];
    histDataMagVerRel = [];
    
    for i = 1:numIters
        pA = cast(datsIt(i).cl, 'double');
        pB = cast(datsIt(i).tl(1), 'double');
        matches = datsIt(i).ma;
        matchesMG = datsM{i, 3}+1;
        matchesMGV = datsM{i, 4}+1;

        nMGV = size(matchesMGV, 2);
        nMG = size(matchesMG, 2);
        
        histDataMagVer = cast([histDataMagVer; repmat([pA, pB],nMGV,1)], 'double');

        nM = int16(round(nMGV/nMG*100));
        histDataMagVerRel = [histDataMagVerRel; repmat([pA, pB],nM,1)];
    end
    
    cnt = hist3(histDataMagVer,{bin_x,bin_y});
    cntRel = hist3(histDataMagVerRel,{bin_x,bin_y});

end