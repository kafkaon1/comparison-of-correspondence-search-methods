clc; clear all;

dataset_name = 'HoloLens_door';
dataPath = fullfile('X:\\CIIRC\\BP\\benchmark\\results\\', dataset_name);
datasetPath = fullfile('X:\\CIIRC\\BP\\benchmark\\', dataset_name);
addpath('X:\CIIRC\BP\solveryM\verify_matches_sources');
toMatch = load(fullfile(datasetPath, 'toMatch.mat')).toMatch;

EPSILON = 6;
K = [1038.135254 0 664.387146; 0 1036.468140 396.142090; 0 0 1];

crashed = 0;

for i=2:size(toMatch,1)
    iA = toMatch(i,1);
    iB = toMatch(i,2);

    c = fullfile(dataPath, sprintf('datI_%d_%d_noVer_r.mat',iA, iB));
    allIters = load(c).a;
    
    imA = fullfile(datasetPath, sprintf('img%d.jpg',iA));
    imB = fullfile(datasetPath, sprintf('img%d.jpg',iB));
    kpA = fullfile(datasetPath, sprintf('ktps%d.mat',iA));
    kpB = fullfile(datasetPath, sprintf('ktps%d.mat',iB));

    outF = fullfile(dataPath, sprintf('datI_%d_%d_LoRA.mat',iA, iB));

    load(kpA); load(kpB);

    imgA = imread(imA);
    imgB = imread(imB);

    sA = size(imgA);
    sB = size(imgB);
    img12 = [imgA imgB];
    
    numIs = size(allIters,1);
    if ~crashed
        allItersUpdated = [];
        il = 1;
    else
        allItersUpdated = load(outF).allItersUpdated;
        il = size(allItersUpdated, 1);
    end

    for i =il:numIs
        fprintf('Iteration %d of %d.\n', i, numIs)

        kptsA = allIters(i).kp1;
        kptsB = allIters(i).kp2;
        matches = allIters(i).ma;
        matchesV = allIters(i).vMa;
        
        correspondences = [kptsA(:, matches(1,:)); kptsB(:, matches(2,:))];
        gVerMatches = [];
        if size(matches,2) > 11
            [gVerMatches, ~] = verify_matches(correspondences(1:2,:), correspondences(3:4,:), ([1;1] * [1:size(correspondences,2)]), 0.8, K, K );
            gVerMatches = gVerMatches(1,:);%verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON);
        end

        
%         % show inliers 
%         figure(); 
%         imshow(img12); hold on;
%         plot(correspondences(1,gVerMatches), correspondences(2,gVerMatches), 'gx', 'MarkerSize', 12, 'LineWidth', 3)
%         plot(correspondences(3, gVerMatches) + sA(2), correspondences(4, gVerMatches), 'gx', 'MarkerSize', 12, 'LineWidth', 3)
%         for i = 1:length(gVerMatches)
%             plot([correspondences(1, gVerMatches(i)) (correspondences(3, gVerMatches(i)) + sA(2))], [correspondences(2, gVerMatches(i)) correspondences(4, gVerMatches(i))], 'g-', 'LineWidth', 2)
%         end
        
        gMatches = matches(:,gVerMatches);
        
        verMatches = verifyMatches(gMatches, kptsA, kptsB,  pvxA, pvxB, EPSILON);
        iter = allIters(i);
        iter.maLo = gMatches;
        iter.maLoV = verMatches;
        allItersUpdated = [ allItersUpdated; iter ];
    end
    
    save(outF, 'allItersUpdated');
end

            

function correctMatches = verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON)
    numCorrMtchs = 0;
    correctA = [];
    correctB = [];
    for i=1:size(matches, 2)
        distsA = (pvxA(1,:)-f1(1,matches(1,i))).^2 + (pvxA(2,:)-f1(2,matches(1,i))).^2;
        distsB = (pvxB(1,:)-f2(1,matches(2,i))).^2 + (pvxB(2,:)-f2(2,matches(2,i))).^2;
        epsA = distsA < EPSILON^2;
        epsB = distsB < EPSILON^2;
        
        k = (epsA == 1 & epsB == 1) ;
        correct = 1;
        if  xor(k, ones(size(k)))
            correct = 0;
        end

        if correct
            % test for duplicate matches
            distsMA = (f1(1,correctA) - f1(1,matches(1,i))).^2 + (f1(2,correctA) - f1(2,matches(1,i))).^2;
            distsMB = (f2(1,correctB) - f2(1,matches(2,i))).^2 + (f2(2,correctB) - f2(2,matches(2,i))).^2;
            epsMA = distsMA < 0.01^2;
            epsMB= distsMB < 0.01^2;
            
            nDuplicate = ~(epsMA == 1 | epsMB == 1);
            if isempty(epsMA) | (nDuplicate)
                numCorrMtchs = numCorrMtchs +1;
                correctA = [correctA matches(1,i)];
                correctB = [correctB matches(2,i)];
            end
        end
    end
    correctMatches = [correctA; correctB];
end 