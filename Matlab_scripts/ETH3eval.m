clear all;
addpath('X:\CIIRC\BP\unc_model_selection\codes')

ethDir = 'X:\CIIRC\BP\ETH3D';
resRootDir = 'X:\CIIRC\BP\ETH3D\results';
eth3DPath = 'X:\\CIIRC\\src\\ETH3DMultiViewEvaluation\\ETH3DMultiViewEvaluation';

D = dir(ethDir);
D = D(~ismember({D.name}, {'.', '..', 'results'}));

fprintf("=> Starting evaluation ETH3D.\n");

ACCS = [];
COMPS = [];
F1S =[];

% SPA13 % SI % WSI2 
evals = { '1SI', '1SIM', '2WSI', '2WSIM', 'SG6', 'SG6M', 'SPA13', 'SPA13M',};
for i = 1:numel(D)
    COMP = [];
    ACC = [];
    F1 = [];
    dataset = D(i);
    fprintf(" > Dataset %s.\n", dataset.name);
    evalPath = fullfile(ethDir, dataset.name, 'eval');
    resPath = fullfile(resRootDir, dataset.name);
    meths = dir(resPath);
    meths = meths(~ismember({meths.name}, {'.', '..'}));
    gt_mlp_path = fullfile(ethDir, dataset.name, 'eval', 'scan_alignment.mlp');
    

    for iM = 1:numel(meths)
        meth = meths(iM);
        if ~any(strcmp(evals,meth.name))
             continue
        end
        % setting 
        mesh_ours_path = fullfile(resPath, meth.name,'dense', 'fused.ply');
        if ~exist(mesh_ours_path, 'file')
            ACC = [ACC; zeros([1,4])];
            COMP = [COMP; zeros([1,4])];
            F1 = [F1; zeros([1,4])];
            continue
        end
        fprintf("   > Method %s.\n", meth.name);
        mesh_ours_aligned_path = fullfile(resPath, meth.name,'dense', 'mesh-aligned.ply');
        colmap_gt_path = fullfile(ethDir, dataset.name, 'calibration');
        colmap_ours_path = fullfile(resPath, meth.name, 'dense', 'sparse');
       
 
        % read models
        [~, gt_images] = read_model(colmap_gt_path);
        [~, ours_images] = read_model(colmap_ours_path);

        % process
        c_gt_images = gt_images.values;
        c_ours_images = ours_images.values;


        % count num. of usages of the images in both reconstr.
        % if count == 2 use it for alignment
        all_images = {};
        all_images_count = [];
        gt_C = [];
        ours_C = [];

        % GT
        for k = 1:length(c_gt_images)
            img = c_gt_images{k};
            img_name = strtrim(strrep(img.name,'dslr_images_undistorted/',''));
            name_found_id = find(strcmp(all_images,img_name));
            if isempty(name_found_id)
                all_images{end+1} = img_name;
                all_images_count(end+1) = 1;
                gt_C = [gt_C -img.R'*img.t];
            else
                all_images_count(name_found_id(1)) = all_images_count(name_found_id(1)) + 1;
            end
        end
        % OURS
        ours_C = zeros(size(gt_C));
        for k = 1:length(c_ours_images)
            img = c_ours_images{k};
            img_name = strtrim(strrep(img.name,'dslr_images_undistorted/',''));
            name_found_id = find(strcmp(all_images,img_name));
            if isempty(name_found_id)
                all_images{end+1} = img_name;
                all_images_count(end+1) = 1;
                ours_C = [ours_C -img.R'*img.t];
                gt_C = [gt_C [0;0;0]];
            else
                all_images_count(name_found_id(1)) = all_images_count(name_found_id(1)) + 1;
                ours_C(:,name_found_id(1)) = -img.R'*img.t;
            end
        end

        % find common images
        common_gt_C = gt_C(:,all_images_count == 2);
        common_ours_C = ours_C(:,all_images_count == 2);

        % find transform
        [~,~,tr] = procrustes(common_gt_C', common_ours_C', 'Reflection',false);
        s = tr.b;
        R = tr.T' * det(tr.T'); 
        t = tr.c(1,:)';

        % load points from ply
        ptCloud = pcread(mesh_ours_path);  %pcshow(ptCloud);
        X = ptCloud.Location';

        % transform points 
        newX = s * R * X + t;
        newptCloud = pointCloud(newX');
        pcwrite(newptCloud,mesh_ours_aligned_path,'PLYFormat','binary');

        % 3) Run the comparision script from ETH3D datasets
        % run the evaluation binaries
        [~, stdout] = system(sprintf('%s --tolerances 0.02,0.05,0.1,0.2 --reconstruction_ply_path %s --ground_truth_mlp_path %s',...
            eth3DPath, mesh_ours_aligned_path, gt_mlp_path));

        % parse and save results
        loc_res = struct();
        loc_res.tolerances = str2num(stdout(strfind(stdout, 'Tolerances:')+12:strfind(stdout, 'Completenesses:')-1));
        loc_res.completenesses = str2num(stdout(strfind(stdout, 'Completenesses:')+15:strfind(stdout, 'Accuracies:')-1));
        loc_res.accuracies = str2num(stdout(strfind(stdout, 'Accuracies:')+11:strfind(stdout, 'F1-scores:')-1));
        loc_res.f1_scores = str2num(stdout(strfind(stdout, 'F1-scores:')+10:end));
        
        %RES  = [loc_res.accuracies;loc_res.completenesses;loc_res.f1_scores];
        %disp(RES);
        ACC = [ACC; loc_res.accuracies];
        COMP = [COMP; loc_res.completenesses];
        F1 = [F1; loc_res.f1_scores];
    end
    ACCS = [ACCS ACC];
    COMPS = [COMPS COMP];
    F1S = [F1S F1];
end

table.data = ACCS*100;
table.tableRowLabels = {'\bfseries SIFT','\bfseries SIFT+MAG','\bfseries Wal+SIFT', '\bfseries Wal+SIFT+MAG', '\bfseries SupGlue', '\bfseries SupGlue+Mag', '\bfseries SpNet','\bfseries SpNet+MAG'};
table.tableBorders = 0;
table.dataFormat = {'%.1f'};
latex = latexTable(table);

table.data = COMPS*100;
latex = latexTable(table);

table.data = F1S*100;
latex = latexTable(table);

