
clc; clear all;
ethDir = 'X:\CIIRC\datasets\'; %'X:\CIIRC\BP\ETH3D';
resRootDir = 'X:\CIIRC\datasets\results';%'X:\CIIRC\BP\ETH3D\results';
D = dir(ethDir);
D = D(ismember({D.name}, { 'ap2'})); %D = D(~ismember({D.name}, {'.', '..', 'results', 'pipes'}));

fprintf("=> Starting SIM FE and FM for ETH3D.\n");
pkT = 0.3;
egT = 12;
MET = 'SI';
SCALE = 0.42;

for i = 1:numel(D)
    dataset = D(i);
    fprintf(" > Dataset %s.\n", dataset.name);
    imPath = fullfile(ethDir, dataset.name, 'images');
    ims = dir(imPath);
    ims = ims(~ismember({ims.name}, {'.', '..'}));
    
    names = {};
    points = {};
    descs = {};
    resDir = fullfile(resRootDir, dataset.name, MET);
    if ~exist(resDir, 'file')
        mkdir(resDir)
    end
    nims = factorial(numel(ims))/(factorial(numel(ims)-2))/2;
    n = 0;
    for iA = 1:numel(ims)
        imA = ims(iA).name;
   
        
        ixA = strcmpi(imA,names);
        if isempty(ixA)| xor(ixA, ones(size(ixA)))
            imgA = imread(fullfile(imPath, imA));
            sO = size(imgA);
            imgA = imresize(imgA, SCALE);
            I1 = single(rgb2gray(imgA));
            [f1,d1] = vl_sift(I1, 'PeakThresh', pkT, 'edgethresh', egT);
            names = [names imA];
            points = [points f1];
            descs = [descs d1];
            
            outFA = sprintf('%s.mat', imA);
            outPA = fullfile(resDir, outFA);
            kpSCALE = sO./size(imgA);
            kp = f1(1:2, :).*[kpSCALE(2); kpSCALE(1)];
            save(outPA, 'kp');
        else
            f1 = points{ixA};
            d1 = descs{ixA};
        end
        
        for iB = iA+1:numel(ims)
            n = n+1;
            fprintf('  Iteration %d/%d.\n', n, nims);
            imB =  ims(iB).name;
            
            outFM = sprintf('%s-%s.mat', imA, imB);
            outPM =  fullfile(resDir, outFM);
            
%             if(iA==iB) || isfile(outPM)
%                 continue
%             end
            
            ixB = strcmpi(imB,names);
            if xor(ixB, ones(size(ixB)))
                imgB = imread(fullfile(imPath, imB));
                sO = size(imgB);
                imgB = imresize(imgB, SCALE);
                I2 = single(rgb2gray(imgB));
                [f2,d2] = vl_sift(I2, 'PeakThresh', pkT, 'edgethresh', egT);
                names = [names imB];
                points = [points f2];
                descs = [descs d2];
               
                outFB = sprintf('%s.mat', imB);
                outPB = fullfile(resDir, outFB);
                kpSCALE = sO./size(imgB);
                kp = f2(1:2, :).*[kpSCALE(2); kpSCALE(1)];
                save(outPB, 'kp');
            else
                f2 = points{ixB};
                d2 = descs{ixB};
            end
            
            [matches, scores] = vl_ubcmatch(d1, d2, 1.5);
            
            addpath('X:\CIIRC\BP\solveryM\relative_pose_solvers');
            
%             correspondences = [f1(1:2, matches(1,:)); f2(1:2, matches(2,:))]';
%             gVerMatches = [];
%             if size(matches,2) >= 7
%                 [score7pc, gVerMatches, model7pc] = vanillaFundamentalMatrixRANSAC(correspondences, 0.98, 1, 0);
%                 %gVerMatches = gVerMatches(1,:);%verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON);
%             end
            
            %%%%%%%%%%%%draw_matches_o(fullfile(imPath, imA),fullfile(imPath, imB), f1(1:2,:).*[kpSCALE(2); kpSCALE(1)], f2(1:2,:).*[kpSCALE(2); kpSCALE(1)], matches)

            save(outPM, 'matches');
        end
    end
            
end
