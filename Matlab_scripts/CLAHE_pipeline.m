clc; clear all;

addpath('X:\CIIRC\src\bm3d_matlab_package\bm3d');

%load data

dataset_name = 'HoloLens_sink';
datpath = fullfile('X:\\CIIRC\\BP\\benchmark\\', dataset_name);
respath = fullfile('X:\\CIIRC\\BP\\benchmark\\results\\', dataset_name);

toMatch = load(fullfile(datpath, 'toMatch.mat')).toMatch;

for i = 1:size(toMatch, 1)
    iA = toMatch(i,1);
    iB = toMatch(i,2);

    imA = fullfile(datpath, sprintf('img%d.jpg',iA));
    imB = fullfile(datpath, sprintf('img%d.jpg',iB));
    kpA = fullfile(datpath, sprintf('ktps%d.mat',iA));
    kpB =fullfile(datpath, sprintf('ktps%d.mat',iB));

    outF =  fullfile(respath, sprintf('datI_%d_%d_Cla_met.mat',iA, iB));
    
    % constants
    EPSILON = 6;
    pT = 0.5;
    eT= 22;
    
    fprintf('=> Starting data extraction for\n   %s, \n   %s, \n   with epsilon of %d.\n', imA, imB, EPSILON);

    load(kpA); load(kpB);

    imgA = imread(imA);
    imgB = imread(imB);

    % denoise
    imgAd = im2uint8(CBM3D(im2double(imgA), 0.09)); 
    imgBd = im2uint8(CBM3D(im2double(imgB), 0.09)); 

    gImgA = rgb2gray(imgAd);
    gImgB = rgb2gray(imgBd);

    sA = size(gImgA);
    sB = size(gImgB);
    gImgAB = [gImgA gImgB];


    %variable parameters
    dists = ["rayleigh"; "uniform"; "exponential"];
    clips = [0.001, 0.003, 0.006, 0.01, 0.03, 0.06, 0.1, 0.3, 0.6, 0.99];
    tls = [[4 4]; [8 8]; [12 12]; [16 16]; [20 20]; [24 24]; [32 32]; [36 36]; [40 40]; [44 44]; [48 48]; [52 52]; [56 56]];
    %wImgA = adapthisteq(gImgA,'clipLimit',clips(3), 'NumTiles', tls(3, :)); %, 'Distribution',dists(1));


    %counter
    allIters = size(dists, 1)*size(clips, 2)*size(tls, 1);
    currIter = 0;

    fprintf('-> Starting CLAHE testing.\n');


    %test vanilla image - reference data
    I1 = single(rgb2gray(imgA));
    I2 = single(rgb2gray(imgB));
    [f1Van,d1Van] = vl_sift(I1,'PeakThresh', pT, 'edgethresh', eT);
    [f2Van,d2Van] = vl_sift(I2,'PeakThresh', pT, 'edgethresh', eT);
    [matchesVan, scoresVan] = vl_ubcmatch(d1Van, d2Van, 1.5);
    verMatVan = verifyMatches(matchesVan, f1Van, f2Van, pvxA, pvxB, EPSILON);

    % experiment metadata
    allData.imA = imA;
    allData.imB = imB;
    allData.kpA = kpA;
    allData.kpB = kpB;
    allData.sA = sA;
    allData.sB = sB;
    allData.eps = EPSILON;
    allData.maVan = matchesVan;
    allData.kpAVan = f1Van(1:2,:);
    allData.kpBVan = f2Van(1:2,:);
    allData.maVan = matchesVan;
    allData.maVanV = verMatVan;

    save(outF, '-struct', 'allData');
    
    

    for id=1:size(dists, 1)
        dist = dists(id, :);

        outFr =  fullfile(respath, sprintf('datI_%d_%d_Cla_%s_r.mat', iA, iB, dist));

        iters = [];
        fprintf(' > Starting with %s\n', dist);
        for ic = 1:size(clips, 2)
            clip = clips(ic);
            for it = 1:size(tls, 1)

                tl = tls(it, :);
                currIter = currIter+1;
                fprintf(' > Iteration %d/%d with clip %d ant tl id %d. \n', currIter, allIters, clip, it);

                %wallis filtering

                wImgA = adapthisteq(gImgA,'clipLimit',clip, 'NumTiles', tl, 'Distribution',dist);
                wImgB = adapthisteq(gImgB,'clipLimit',clip, 'NumTiles', tl, 'Distribution',dist);
                I1 = single(wImgA);
                I2 = single(wImgB);

                %feature extraction and matching
                [f1,d1] = vl_sift(I1,'PeakThresh', pT, 'edgethresh', eT);
                [f2,d2] = vl_sift(I2,'PeakThresh', pT, 'edgethresh', eT);
                [matches, scores] = vl_ubcmatch(d1, d2, 1.5);

                verMatches = verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON);

                iterData.cl = clip;
                iterData.tl = tl;
                iterData.kp1 = f1(1:2,:);
                iterData.kp2 = f2(1:2,:);
                iterData.ma = matches;
                iterData.vMa = verMatches;

                sma = size(verMatches, 2);

                fprintf('    Found %d VM.\n', sma); 

                iters = [iters; iterData];

        %         figure();
        %         imshow(gImgAB); hold on;
        %         
        %         matches = verMatches;
        %         plot(f1(1,matches(1,:)), f1(2, matches(1,:)), 'b.', 'MarkerSize', 5)
        %         plot((f2(1,matches(2,:)) + sA(2)), f2(2,matches(2,:)), 'b.', 'MarkerSize', 5)
        %         for i = 1:numel(matches(1,:))
        %             plot([f1(1,matches(1,i)) (f2(1,matches(2,i)) + sA(2))], [f1(2, matches(1,i)) f2(2,matches(2,i))], 'g', 'LineWidth', 0.4);
        %         end
            end
        end

        fprintf('-> Distribution testing ended.\n');
        save(outFr, 'iters');

    end
end

%==========VERIFY=======================================================================================================
function correctMatches = verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON)
    numCorrMtchs = 0;
    correctA = [];
    correctB = [];
    for i=1:size(matches, 2)
        distsA = (pvxA(1,:)-f1(1,matches(1,i))).^2 + (pvxA(2,:)-f1(2,matches(1,i))).^2;
        distsB = (pvxB(1,:)-f2(1,matches(2,i))).^2 + (pvxB(2,:)-f2(2,matches(2,i))).^2;
        epsA = distsA < EPSILON^2;
        epsB = distsB < EPSILON^2;
        
        k = (epsA == 1 & epsB == 1) ;
        correct = 1;
        if  xor(k, ones(size(k)))
            correct = 0;
        end

        if correct
            % test for duplicate matches
            distsMA = (f1(1,correctA) - f1(1,matches(1,i))).^2 + (f1(2,correctA) - f1(2,matches(1,i))).^2;
            distsMB = (f2(1,correctB) - f2(1,matches(2,i))).^2 + (f2(2,correctB) - f2(2,matches(2,i))).^2;
            epsMA = distsMA < 0.01^2;
            epsMB= distsMB < 0.01^2;
            
            nDuplicate = ~(epsMA == 1 & epsMB == 1);
            if isempty(epsMA) | (nDuplicate)
                numCorrMtchs = numCorrMtchs +1;
                correctA = [correctA matches(1,i)];
                correctB = [correctB matches(2,i)];
            end
        end
    end
    correctMatches = [correctA; correctB];
end