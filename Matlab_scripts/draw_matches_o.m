function draw_matches_o(imA,imB, kptsA, kptsB, matches)

    imgA = imread(imA);
    imgB = imread(imB);
    imgAB = [ imgA imgB];
    
    sA = size(imgA);
    
    figure();
    imshow(imgAB); hold on;

    plot(kptsA(1,matches(1,:)), kptsA(2, matches(1,:)), 'b.', 'MarkerSize', 5)
    plot((kptsB(1,matches(2,:)) + sA(2)), kptsB(2,matches(2,:)), 'b.', 'MarkerSize', 5)
    for i = 1:numel(matches(1,:))
        plot([kptsA(1,matches(1,i)) (kptsB(1,matches(2,i)) + sA(2))], [kptsA(2, matches(1,i)) kptsB(2,matches(2,i))], 'g', 'LineWidth', 0.4);
    end
    print('a')
end

