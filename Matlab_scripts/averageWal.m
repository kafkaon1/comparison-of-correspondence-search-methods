
clc; clear all;
benchdir = 'X:\CIIRC\BP\benchmark';
D = dir(benchdir);
D = D(~ismember({D.name}, {'.', '..', 'results'}));

fprintf("=> Starting average benchmarking.\n");

bin_x = linspace(0, 4, 41);
bin_y = round(linspace(0.1,1,10),1);
xBounds = [0.1, 3.1];
yBounds = [0.1,1];

sumVans = 0;
sumHist = zeros([size(bin_x, 2),size(bin_y, 2)]);

outperformers = zeros([size(bin_x, 2),size(bin_y, 2)]);
nVans = struct();
hists = struct();
n = 0;

TSH = 0.1;

for i = 1:numel(D)
    file = D(i);
    fprintf(" > Starting dataset %s.\n", file.name);
    toMatch = load(fullfile(file.folder, file.name, 'toMatch.mat')).toMatch ;
    for j = 1:size(toMatch,1)
        iA = toMatch(j,1);
        iB = toMatch(j,2);

        fprintf("  >Matching %d-th combination : %d-%d.\n", j, iA, iB);
        
        %get paths
        respath = fullfile('X:\CIIRC\BP\benchmark\results\', file.name);
        walMetadata =  fullfile(respath, sprintf('datI_%d_%d_Wall_met.mat',iA, iB));
        walIters =  fullfile(respath, sprintf('datI_%d_%d_Wall_r.mat',iA, iB));
        walMag =  fullfile(respath, sprintf('datI_%d_%d_Wall_MAG.mat',iA, iB));
        walMetMag = fullfile(respath, sprintf('datI_%d_%d_Wall_met_MAG.mat',iA, iB));
        
        %load data
        datsMet = load(walMetadata);
        datsIt = load(walIters).iters;
        datsM = load(walMag).itersUpdated;
        datsVan = load(walMetMag).meta;

        %check for errors
        if size(datsIt, 1) == size(datsM, 1)
            numIters = size(datsIt, 1);
        else
            error('Invalid input sizes')
        end
        
        nVan = size(datsVan{3},2);
        nVanm = size(datsVan{2},2);
        nV = int16(round(nVan/nVanm*100));
        
        [histDataMagVer, histDataMagVerRel] = createHist(datsIt, datsM, numIters, bin_x, bin_y);
        itername = sprintf('%s%d', file.name, j);
        
        nVans.(itername) = nVan;
        sumVans = sumVans + nVan;
        hists.(itername) = histDataMagVer;
        sumHist = sumHist + histDataMagVer;
        n = n+1;
        
    end
end

% visualisation

iternames = fieldnames(hists);

avgH = sumHist/n;
avgVan = sumVans/n;

for i=1:numel(iternames)
    nV = nVans.(iternames{i});
    sumH = hists.(iternames{i});
    outperformers = outperformers + (sumH > nV);
end

outperformers = uint16(outperformers./n.*100);

% Plot frequency values with surf
[x,y] = meshgrid(bin_x,bin_y);

d = (avgH-avgVan)';
p = d > TSH;
d(p) = d(p) + 20;
n = d < -TSH;
d(n) = -10;
z = d <= TSH & d>= -TSH;
d(z) = 0;
figure();
hold on;

for i = 1 : size(bin_x, 2)
    for j = 1: size(bin_y, 2)
        pA = bin_x(i);
        pB = bin_y(j);
        if (pA >= xBounds(1)) && (pA < xBounds(2)) && (pB >= yBounds(1)) && (pB < yBounds(2))
            txt = sprintf('%d',uint16((avgH(i,j)/avgVan-1)*100));
            if d(j, i) > 0
                text(pA+0.005,pB + 0.05,txt, 'FontSize', 11.5);
            elseif d(j,i)< 0
                text(pA+0.005,pB + 0.05,'-', 'FontSize', 13, 'FontWeight', 'bold');
            else
                text(pA+0.005,pB + 0.05,'0', 'FontSize', 13, 'FontWeight', 'bold');
            end
         end
    end
end
   


surf(x,y,d);
view(2)
zlabel('Difference of number of matches');
title('Average diff. of matches of prepr. and not prepr. img.', 'Fontweight','Bold')
xlabel('parameter A');
ylabel('parameter B');
s = findobj(gca,'Type','Surface');
s.FaceAlpha = 0.5;
xlim(xBounds)
ylim(yBounds)
%colorbar;
grid on;
set(gca,'FontSize',13);
set(gca,'Linewidth',1.1);
set(gcf,'Position', [100,100,750,600]);

figure();
hold on;


for i = 1 : size(bin_x, 2)
    for j = 1: size(bin_y, 2)
        pA = bin_x(i);
        pB = bin_y(j);
        if (pA >= xBounds(1)) && (pA < xBounds(2)) && (pB >= yBounds(1)) && (pB < yBounds(2))
            txt = sprintf('%d', outperformers(i,j));
            text(pA+0.005,pB + 0.05,txt, 'FontSize', 11.5);%, 'FontWeight', 'bold');
        end

    end
end
   


surf(x,y,outperformers');
view(2)
zlabel('Difference of number of matches');
title('Number of better performing prepr. than not prepr. img.', 'Fontweight','Bold')
xlabel('parameter A');
ylabel('parameter B');
s = findobj(gca,'Type','Surface');
s.FaceAlpha = 0.5;
xlim(xBounds)
ylim(yBounds)
%colorbar;
grid on;
set(gca,'FontSize',13);
set(gca,'Linewidth',1.1);
set(gcf,'Position', [100,100,750,600]);

   

function [cnt, cntRel] = createHist(datsIt, datsM, numIters, bin_x, bin_y)

    histDataMagVer = [];
    histDataMagVerRel = [];
    
    for i = 1:numIters
        pA = cast(datsIt(i).pA, 'double');
        pB = cast(datsIt(i).pB, 'double');
        matchesMG = datsM{i, 3}+1;
        matchesMGV = datsM{i, 4}+1;

        nMGV = size(matchesMGV, 2);
        nMG = size(matchesMG, 2);

        histDataMagVer = cast([histDataMagVer; repmat([pA, pB],nMGV,1)], 'double');

        nM = int16(round(nMGV/nMG*100));
        histDataMagVerRel = [histDataMagVerRel; repmat([pA, pB],nM,1)];

    end
    
    
    cnt = hist3(histDataMagVer,{bin_x,bin_y});
    cntRel = hist3(histDataMagVerRel,{bin_x,bin_y});

end