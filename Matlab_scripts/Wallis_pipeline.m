clc; %clear all;

addpath('X:\CIIRC\src\bm3d_matlab_package\bm3d');

%load data

dataset_name = 'HoloLens_kancl';
datpath = fullfile('X:\\CIIRC\\BP\\benchmark\\', dataset_name);
respath = fullfile('X:\\CIIRC\\BP\\benchmark\\results\\', dataset_name);

if ~exist(respath, 'file')
    mkdir(respath)
end

toMatch = load(fullfile(datpath, 'toMatch.mat')).toMatch ;

for i = 1:size(toMatch, 1)
    iA = toMatch(i,1);
    iB = toMatch(i,2);

    imA = fullfile(datpath, sprintf('img%d.jpg',iA));
    imB = fullfile(datpath, sprintf('img%d.jpg',iB));
    kpA = fullfile(datpath, sprintf('ktps%d.mat',iA));
    kpB = fullfile(datpath, sprintf('ktps%d.mat',iB));

    outF =  fullfile(respath, sprintf('datI_%d_%d_Wall_met.mat',iA, iB));
    outFr =  fullfile(respath, sprintf('datI_%d_%d_Wall_r.mat',iA, iB));
    outFrW =  fullfile(respath, sprintf('datI_%d_%d_Wall_rW.mat',iA, iB));

    load(kpA); load(kpB);
    
    fprintf('=> Starting data extraction for\n   %s, \n   %s.\n', imA, imB);

    imgA = imread(imA);
    imgB = imread(imB);

    %denoise
    disp('-> Denoising.')
    imgAd = im2uint8(CBM3D(im2double(imgA), 0.11)); 
    imgBd = im2uint8(CBM3D(im2double(imgB), 0.11)); 

    gImgA = rgb2gray(imgAd);
    gImgB = rgb2gray(imgBd);
    
    sA = size(gImgA);
    sB = size(gImgB);
    gImgAB = [gImgA gImgB];

    % vaiable parameters
    parA = linspace(0, 4, 41);
    parB = round(linspace(0.1,1,10),1);
    winWidth = linspace(3,33,31);

    % constants
    EPSILON = 6;
    MEAN = 127;
    STDEV = 60;
    pT = 0.3;
    eT = 12;
    
    %counter
    allIters = size(parA, 2)*size(parB, 2);
    currIter = 0;

    fprintf('-> Starting PA and PB testing.\n');

    %test vanilla image - reference data
    I1 = single(rgb2gray(imgA));
    I2 = single(rgb2gray(imgB));
    [f1Van,d1Van] = vl_sift(I1,'PeakThresh', pT, 'edgethresh', eT);
    [f2Van,d2Van] = vl_sift(I2,'PeakThresh', pT, 'edgethresh', eT);
    [matchesVan, scoresVan] = vl_ubcmatch(d1Van, d2Van, 1.5);
    verMatVan = verifyMatches(matchesVan, f1Van, f2Van, pvxA, pvxB, EPSILON);

    % experiment metadata
    allData.imA = imA;
    allData.imB = imB;
    allData.kpA = kpA;
    allData.kpB = kpB;
    allData.sA = sA;
    allData.sB = sB;
    allData.eps = EPSILON;
    allData.mean = MEAN;
    allData.stdev = STDEV;
    allData.pA = parA;
    allData.pB = parB;
    allData.maVan = matchesVan;
    allData.kpAVan = f1Van(1:2,:);
    allData.kpBVan = f2Van(1:2,:);
    allData.maVan = matchesVan;
    allData.maVanV = verMatVan;

    save(outF, '-struct', 'allData');

    iters = [];
    bestAB = [0 0 0];

    for iPA=1:size(parA, 2)
        pA = parA(iPA);
        for iPB = 1:size(parB, 2)
            pB = parB(iPB);

            currIter = currIter+1;
            fprintf(' > Iteration %d/%d with PA %d and PB %d\n', currIter, allIters, pA, int8(pB*10));

            %wallis filtering
            wImgA = WallisFilter(gImgA, MEAN, STDEV, pA, pB, 14, false);
            wImgB = WallisFilter(gImgB, MEAN, STDEV, pA, pB, 14, false);
            I1 = single(wImgA);
            I2 = single(wImgB);

            %feature extraction and matching
            [f1,d1] = vl_sift(I1,'PeakThresh', pT, 'edgethresh', eT);
            [f2,d2] = vl_sift(I2,'PeakThresh', pT, 'edgethresh', eT);
            [matches, scores] = vl_ubcmatch(d1, d2, 1.5);

            verMatches = verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON);

            iterData.pA = pA;
            iterData.pB = pB;
            iterData.kp1 = f1(1:2,:);
            iterData.kp2 = f2(1:2,:);
            iterData.ma = matches;
            iterData.vMa = verMatches;

            sma = size(verMatches, 2);

            fprintf('    Found %d VM.\n', sma); 

            iters = [iters; iterData];

            %store best value for wW testing
            if sma > bestAB(3)
                bestAB = [pA pB sma];
            end

    %         figure();
    %         imshow(gImgAB); hold on;
    %         
    %         matches = verMatches;
    %         plot(f1(1,matches(1,:)), f1(2, matches(1,:)), 'b.', 'MarkerSize', 5)
    %         plot((f2(1,matches(2,:)) + sA(2)), f2(2,matches(2,:)), 'b.', 'MarkerSize', 5)
    %         for i = 1:numel(matches(1,:))
    %             plot([f1(1,matches(1,i)) (f2(1,matches(2,i)) + sA(2))], [f1(2, matches(1,i)) f2(2,matches(2,i))], 'g', 'LineWidth', 0.4);
    %         end

        end
    end

    fprintf('-> PA and PB testing ended.\n-> Starting WW testing.\n');
    save(outFr, 'iters');
    %take best previous parameters
    pA = bestAB(1);
    pB = bestAB(2);

    bestW = [0 0];
    itersW = [];
    for iW=1:size(winWidth, 2)
        wW = winWidth(iW);
        currIter = currIter+1;
        fprintf(' > Iteration %d/%d with WW %d\n', iW, size(winWidth, 2), wW);

        %wallis filtering
        wImgA = WallisFilter(gImgA, MEAN, STDEV, pA, pB, wW, false);
        wImgB = WallisFilter(gImgB, MEAN, STDEV, pA, pB, wW, false);

        I1 = single(wImgA);
        I2 = single(wImgB);

        %feature extraction
        [f1,d1] = vl_sift(I1, 'PeakThresh', pT, 'edgethresh', eT);
        [f2,d2] = vl_sift(I2, 'PeakThresh', pT, 'edgethresh', eT);
        [matches, scores] = vl_ubcmatch(d1, d2, 1.5);

        verMatches = verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON);

        iterData.W = wW;
        iterData.kp1 = f1(1:2,:);
        iterData.kp2 = f2(1:2,:);
        iterData.ma = matches;
        iterData.vMa = verMatches;

        sma = size(verMatches, 2);

        fprintf('    Found %d VM.\n', sma); 
        itersW = [itersW; iterData];

        %store best value for wW testing
        if sma > bestW(2)
            bestW = [wW sma];
        end
    end

    fprintf('-> WW testing ended, best W: %d.\n', bestW(1))
    save(outFrW,  'itersW');
    fprintf('=> Data saved, process finished, best values are PA %d, PB %d, and WW %d.\n', bestAB(1), bestAB(2), bestW(1));

end

%==========VERIFY=======================================================================================================
function correctMatches = verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON)
    numCorrMtchs = 0;
    correctA = [];
    correctB = [];
    for i=1:size(matches, 2)
        distsA = (pvxA(1,:)-f1(1,matches(1,i))).^2 + (pvxA(2,:)-f1(2,matches(1,i))).^2;
        distsB = (pvxB(1,:)-f2(1,matches(2,i))).^2 + (pvxB(2,:)-f2(2,matches(2,i))).^2;
        epsA = distsA < EPSILON^2;
        epsB = distsB < EPSILON^2;
        
        k = (epsA == 1 & epsB == 1) ;
        correct = 1;
        if  xor(k, ones(size(k)))
            correct = 0;
        end

        if correct
            % test for duplicate matches
            distsMA = (f1(1,correctA) - f1(1,matches(1,i))).^2 + (f1(2,correctA) - f1(2,matches(1,i))).^2;
            distsMB = (f2(1,correctB) - f2(1,matches(2,i))).^2 + (f2(2,correctB) - f2(2,matches(2,i))).^2;
            epsMA = distsMA < 0.01^2;
            epsMB= distsMB < 0.01^2;
            
            nDuplicate = ~(epsMA == 1 & epsMB == 1);
            if isempty(epsMA) | (nDuplicate)
                numCorrMtchs = numCorrMtchs +1;
                correctA = [correctA matches(1,i)];
                correctB = [correctB matches(2,i)];
            end
        end
    end
    correctMatches = [correctA; correctB];
end


%============WALLIS======================================================================================================
function outputImage = WallisFilter(inputImage, desiredMean, desiredStdDev, Amax, percentage, windowWidth, preSmooth)
outputImage = []; % Initialize
% Make sure the window width is odd.  If it's not, add 1 to make it odd.
if mod(windowWidth, 2) == 0
	% For example, if windowWidth = 4, make it 5 instead.
	windowWidth = windowWidth+1;
end
% Get a double copy of the image.
dblImage = double(inputImage); % Cast to double from whatever it is on input.

% Optionally, smooth the image with a gaussian blur beforehand, if specified.
if preSmooth
	% gauss_blur = 1/273 * [1 4 7 4 1; 4 16 26 16 4; 7 26 41 26 7; 4 16 26 16 4; 1 4 7 4 1];
	gauss_blur = fspecial('gaussian', windowWidth, 1);
	dblImage = conv2(dblImage, gauss_blur, 'same');
end

% Make an image of all ones the same size as the input image.
uniformImage = ones(size(dblImage));
% Make a convolution kernel of all ones to integrate the gray level within the window.
kernel = ones(windowWidth);
% Make a new image where pixel value is the sum of gray levels within the sliding window.
sumImage = conv2(dblImage, kernel, 'same');
% Make a new image where pixel value is the COUNT of pixels within the sliding window.  Will be smaller near the edges of the image.
countImage = conv2(uniformImage, kernel, 'same');

% Get the sliding mean image.
localMeanImage = sumImage ./ countImage;
% Compute the variance image = <(x-xBar).^2> = second central moment.
D = conv2((dblImage - localMeanImage).^2, kernel, 'same') ./ countImage; %(windowWidth^2); %;
% Compute local standard deviation by taking the square root of the variance image.
D = sqrt(D);
% Apply the Wallis formula scaling.

G = (dblImage - localMeanImage) .*  desiredStdDev* Amax  ./ (Amax + D ) + percentage * desiredMean + (1-percentage) * localMeanImage;

%G = (dblImage - localMeanImage) .* Amax .* desiredStdDev ./ (Amax .* D + desiredStdDev) + percentage * desiredMean + (1-percentage) * localMeanImage;

% Clip values.
% Don't let image be less than 0.
G(G < 0) = 0;
% Don't let image be more than the max.
maxPossibleGrayLevel = intmax(class(inputImage));
G(G > maxPossibleGrayLevel) = maxPossibleGrayLevel;

% Cast back to the same class as the input image.
outputImage = cast(G, 'like', inputImage);
end