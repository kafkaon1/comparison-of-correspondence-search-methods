dataset_name = 'HoloLens_hall_down';
datpath = fullfile('X:\\CIIRC\\BP\\benchmark\\', dataset_name);
respath = fullfile('X:\\CIIRC\\BP\\benchmark\\results\\', dataset_name);

iA = 273;
iB = 282;
imA = fullfile(datpath, sprintf('img%d.jpg',iA));
imB = fullfile(datpath, sprintf('img%d.jpg',iB));
kpA = fullfile(datpath, sprintf('ktps%d.mat',iA));
kpB = fullfile(datpath, sprintf('ktps%d.mat',iB));

pkT = 0.3;
egT = 12;

load(kpA); load(kpB);

imgA = imread(imA);
imgB = imread(imB);

sA = size(imgA);
sB = size(imgB);
imgAll = [imgA imgB;imgA imgB;imgA imgB];

%     figure()
%     imshow(imgA); hold on;
%     plot(pvxA(1,:), pvxA(2, :), 'b.', 'MarkerSize', 3)

%     iR = randi([0 size(pvxA,2)],1,30);

% figure();
% imshow(img12); hold on;
% plot(pvxA(1,iR), pvxA(2, iR), 'b.', 'MarkerSize', 5)
% plot((pvxB(1, iR) + sA(2)), pvxB(2,iR), 'b.', 'MarkerSize', 5)
% for i = 1:numel(iR)
%     plot([pvxA(1,iR(i)) (pvxB(1, iR(i)) + sA(2))], [pvxA(2, iR(i)) pvxB(2,iR(i))], 'g', 'LineWidth', 0.3);
% end

I1 = single(rgb2gray(imgA));
I2 = single(rgb2gray(imgB));


%feature extraction
[f1,d1] = vl_sift(I1, 'PeakThresh', pkT, 'edgethresh', egT);
[f2,d2] = vl_sift(I2, 'PeakThresh', pkT, 'edgethresh', egT);
[matches, scores] = vl_ubcmatch(d1, d2, 1.5);

addpath('X:\CIIRC\BP\solveryM\relative_pose_solvers');
correspondences = [f1(1:2, matches(1,:)); f2(1:2, matches(2,:))]';
gVerMatches = [];
if size(matches,2) >= 7
    [score7pc, gVerMatches, model7pc] = vanillaFundamentalMatrixRANSAC(correspondences, 0.98, 1, 0);
    %gVerMatches = gVerMatches(1,:);%verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON);
end


gMatches = matches(:,gVerMatches);

%verMatches = verifyMatches(gMatches, f1, f2, pvxA, pvxB, EPSILON);

figure()
imshow(imgAll); hold on;

plot(f1(1,:), f1(2, :) , 'b.', 'MarkerSize', 5)
plot((f2(1,:) + sA(2)), f2(2,:), 'b.', 'MarkerSize', 5)

plot(f1(1,matches(1,:)), f1(2, matches(1,:)) + sA(1), 'b.', 'MarkerSize', 3.8)
plot((f2(1,matches(2,:)) + sA(2)), f2(2,matches(2,:))  + sA(1), 'b.', 'MarkerSize', 3.8)
for i = 1:numel(matches(1,:))
    lp = plot([f1(1,matches(1,i)) (f2(1,matches(2,i)) + sA(2))], [f1(2, matches(1,i)) f2(2,matches(2,i))]+ sA(1), 'g', 'LineWidth', 0.4);
    lp.Color = [0 1 0 0.3];
end

matches = gMatches; %verMatches;

plot(f1(1,matches(1,:)), f1(2, matches(1,:)) + 2*sA(1), 'b.', 'MarkerSize', 3.8)
plot((f2(1,matches(2,:)) + sA(2)), f2(2,matches(2,:))  + 2*sA(1), 'b.', 'MarkerSize', 3.8)
for i = 1:numel(matches(1,:))
    lp = plot([f1(1,matches(1,i)) (f2(1,matches(2,i)) + sA(2))], [f1(2, matches(1,i)) f2(2,matches(2,i))]+ 2*sA(1), 'g', 'LineWidth', 0.4);
    lp.Color = [0 1 0 0.3];
end

res = 600;
print('resized.jpeg', ['-r' num2str(res)]);
  