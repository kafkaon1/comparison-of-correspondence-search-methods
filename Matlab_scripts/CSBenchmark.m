clc; clear all;

benchdir = 'X:\CIIRC\BP\benchmark';
D = dir(benchdir);
D = D(~ismember({D.name}, {'.', '..', 'results'}));

fprintf("=> Starting benchmarking.\n");

numMeth = 5;
numDts = size(D,1);

%grid 
grid.sIm = [756,1344];
grid.size = [9, 12];
grid.max = 5;
grid.const = grid.sIm(1:2)./grid.size;

% final results
results = struct();
for i = 1:numel(D)
    file = D(i);
    fprintf(" > Starting dataset %s.\n", file.name);
    toMatch = load(fullfile(file.folder, file.name, 'toMatch.mat')).toMatch ;
    datRes = {};
    if file.name == "HoloLens_kancl"
        continue
    end
    for j = 1:size(toMatch,1)
        iA = toMatch(j,1);
        iB = toMatch(j,2);
        
        fprintf("  >Running for %d-th combination : %d-%d.\n", j, iA, iB);
        
        imA = fullfile(file.folder, file.name, sprintf('img%d.jpg', iA));
        imB = fullfile(file.folder, file.name, sprintf('img%d.jpg', iB));
        %kptA = load(fullfile(datpath, sprintf('ktps%d.mat',iA)));
        %kptB = load(fullfile(datpath, sprintf('ktps%d.mat',iB)));
        
        imPairRes.SG = deal(resSG(file.name, iA, iB, grid, 0));
        imPairRes.SGM = resSG(file.name, iA, iB, grid, 1);
        imPairRes.SP = resSP(file.name, iA, iB, grid);
        imPairRes.SIM = resSIM(file.name, iA, iB, grid);
        imPairRes.WSIM = resWSIM(file.name, iA, iB, grid);
        
        imPairName = sprintf('i%d_i%d', iA, iB);
        
        datRes.(imPairName) = imPairRes;
    end
    results.(file.name) = datRes;
end

%RES = setUpResultsTable(results, numMeth);
RES = setUpResultsTableAvg(results, numMeth)
%save results as .tex table
table.data = RES;
table.tableRowLabels = {'\bfseries SIFT+MAG','\bfseries Wallis+SIFT+MAG','\bfseries SuperGlue','\bfseries SuperGlue + MAG', '\bfseries SparseNCNet',};
table.tableBorders = 0;
table.dataFormat = {'%.1f'};
latex = latexTable(table);

fid=fopen('ResultTable.tex','w');
[nrows,ncols] = size(latex);
for row = 1:nrows
    fprintf(fid,'%s\n',latex{row,:});
end
fclose(fid);


% ======= FUNCTIONS FOR OBTAINING DATA OF EACH METHOD =======
function ret = resSG(name, idA, idB, gridDat, M)
    %best pars
    bP = [1e-4, 0.010];
    iDw =39;%31;% 64;
    if ~M
        iDw = 53;%142;%31;
    end        
    
    %load data
    respath = fullfile('X:\CIIRC\BP\benchmark\results\', name);
    sgMag = load(fullfile(respath, sprintf('datI_%d_%d_SG_MAG_a.mat',idA, idB))).itersUpdated;
    sg = load(fullfile(respath, sprintf('datI_%d_%d_SG_r.mat',idA, idB))).itersSG;

%         pars = round([sg{:,1}; sg{:,2}],1,'significant');
%         [iDw, ~, ~] = intersect(cast(bP, 'double'),cast(pars, 'double'),'rows');

    kpA = sg{iDw, 3}'.*[1344/640; 756/480];
    kpB = sg{iDw,4}'.*[1344/640; 756/480];
    mMG = sgMag{iDw,3}+1;
    mMGV = sgMag{iDw,4}+1;
    m = sgMag{iDw,5}+1;
    mV = sgMag{iDw,6}+1;

    if M
        abs = size(mMGV, 2);
        rel = abs/size(mMG, 2)*100;
        %draw_matches_v(imA,imB, kptsA, kptsB, matches, mV)
        dist = countSpreadGrid(gridDat, kpA, kpB, mMGV);
    else
        abs = size(mV, 2);
        rel = abs/size(m, 2)*100;
        dist = countSpreadGrid(gridDat, kpA, kpB, mV);
    end
    ret = [abs, rel, dist];
end

function ret = resSP(file, idA, idB, gridDat)
    %best par
    bP = [1e-4, 0.010];
    iDw = 18;
    
    %load data
    respath = fullfile('X:\CIIRC\BP\benchmark\results\', file);
    spMag = load(fullfile(respath, sprintf('datI_%d_%d_SP_MAG.mat',idA, idB))).itersUpdated;
    sp = load(fullfile(respath, sprintf('datI_%d_%d_SP_r.mat',idA, idB))).SPmatches;
    
    kpA = sp{1}';
    kpB = sp{2}';
    mMG = spMag{iDw,2}+1;
    mMGV = spMag{iDw,3}+1;

    abs = size(mMGV, 2);
    rel = abs/size(mMG, 2)*100;
    %draw_matches_v(imA,imB, kptsA, kptsB, matches, mV)
    dist = countSpreadGrid(gridDat, kpA, kpB, mMGV);
    
    ret = [abs, rel, dist];
    
end

function ret = resSIM(file, idA, idB, gridDat)
    %best pars
    bP = [12, 0.3];
    iDw = 359;
    
    %load data
    respath = fullfile('X:\CIIRC\BP\benchmark\results\', file);
    dMag = load(fullfile(respath, sprintf('datI_%d_%d_MAG.mat',idA, idB))).itersUpdated;
    dLor = load(fullfile(respath, sprintf('datI_%d_%d_LoRA_r.mat',idA, idB))).a;
    
    kpA = dLor(iDw).kp1;
    kpB = dLor(iDw).kp2;
    mMG = dMag{iDw,3}+1;
    mMGV = dMag{iDw,4}+1;
    
    eT = dMag{iDw,1};
    pT = dMag{iDw,2};

    abs = size(mMGV, 2);
    rel = abs/size(mMG, 2)*100;
    %draw_matches_v(imA,imB, kptsA, kptsB, matches, mV)
    dist = countSpreadGrid(gridDat, kpA, kpB, mMGV);
    
    ret = [abs, rel, dist];

end

function ret = resWSIM(file, idA, idB, gridDat)
    %best par
    bP = [1e-4, 0.010];
    iDw = 226;
    
    %load data
    respath = fullfile('X:\CIIRC\BP\benchmark\results\', file);
    walMag =  load(fullfile(respath, sprintf('datI_%d_%d_Wall_MAG.mat',idA, idB))).itersUpdated;
	wal = load(fullfile(respath, sprintf('datI_%d_%d_Wall_r.mat',idA, idB))).iters;
    
    kpA = wal(iDw).kp1;
    kpB = wal(iDw).kp2;
    mMG = walMag{iDw,3}+1;
    mMGV = walMag{iDw,4}+1;

    abs = size(mMGV, 2);
    rel = abs/size(mMG, 2)*100;
    %draw_matches_v(imA,imB, kptsA, kptsB, matches, mV)
    dist = countSpreadGrid(gridDat, kpA, kpB, mMGV);
    
    ret = [abs, rel, dist];

end

% ======= UTILITY FUNCTIONS  ==============================

function numSprMat = countSpreadGrid(grid, kpA, kpB, matches)
    countsA = zeros(grid.size);
    countsB = zeros(grid.size); 
    for iMatch = 1:size(matches,2)
        kA = kpA(:, matches(1,iMatch));
        iA = floor([kA(2), kA(1)]./grid.const)+1;
        kB = kpB(:, matches(2,iMatch));
        
        iB = floor([kB(2), kB(1)]./grid.const)+1;
        if countsA(iA(1), iA(2)) < grid.max
            countsA(iA(1), iA(2)) = countsA(iA(1), iA(2)) + 1;
        end
        if countsB(iB(1), iB(2)) < grid.max
            countsB(iB(1), iB(2)) = countsB(iB(1), iB(2)) + 1;
        end
    end
    
    numSprMat = (sum(sum(countsA)) + sum(sum(countsB)))/2;
end


function draw_matches_v(imA,imB, kptsA, kptsB, matches, mV)

    imgA = imread(imA);
    imgB = imread(imB);
    imgAB = [ imgA imgB];
    
    sA = size(imgA);
    
    figure();
    imshow(imgAB); hold on;

    plot(kptsA(1,matches(1,:)), kptsA(2, matches(1,:)), 'b.', 'MarkerSize', 5)
    plot((kptsB(1,matches(2,:)) + sA(2)), kptsB(2,matches(2,:)), 'b.', 'MarkerSize', 5)
    for i = 1:numel(matches(1,:))
        if isVerified(matches(:,i), mV)
            color = 'g';
        else
            color = 'r';
        end
        plot([kptsA(1,matches(1,i)) (kptsB(1,matches(2,i)) + sA(2))], [kptsA(2, matches(1,i)) kptsB(2,matches(2,i))], color, 'LineWidth', 0.4);
    end
    print('a')
end

function isV = isVerified(match, mV)
    [C, ~, ~] = intersect(cast(match, 'int64')',cast(mV, 'int64')' ,'rows');
    if C
        isV = 1;
    else
        isV = 0;
    end
end

function RES = setUpResultsTable(results, numMeth)
    datNames = fieldnames(results);
    RES = [];
    for i = 1:size(datNames)
        datName = datNames{i};
        datRes = results.(datName);
        
        itNames = fieldnames(datRes);
        resCol = zeros(numMeth, 3);
        for j = 1:size(itNames)
            itName = itNames{j};
            it = datRes.(itName);
            methDat = [it.SIM; it.WSIM; it.SG; it.SGM; it.SP];
            resCol = resCol + methDat;
        end
        resCol = resCol/j;
        RES = [RES resCol];
        if i==3
            break
        end
    end
end

function RES = setUpResultsTableAvg(results, numMeth)
    datNames = fieldnames(results);
    RES = zeros(numMeth, 3);
    n = 0;
    for i = 1:size(datNames)
        datName = datNames{i};
        datRes = results.(datName);
        
        itNames = fieldnames(datRes);
        for j = 1:size(itNames)
            itName = itNames{j};
            it = datRes.(itName);
            methDat = [it.SIM; it.WSIM; it.SG; it.SGM; it.SP];
            RES = RES + methDat;
            n = n+1;
        end
        
    end
    RES = RES/n;
    
end