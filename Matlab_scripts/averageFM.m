clc; clear all;
benchdir = 'X:\CIIRC\BP\benchmark';
D = dir(benchdir);
D = D(~ismember({D.name}, {'.', '..', 'results'}));

fprintf("=> Starting average benchmarking.\n");

bin_x = linspace(0, 10, 401);
bin_y = linspace(2,30,29);

sumM = zeros([size(bin_x, 2),size(bin_y, 2)]);
sumMRel = zeros([size(bin_x, 2),size(bin_y, 2)]);
sumMN = zeros([size(bin_x, 2),size(bin_y, 2)]);
sumL = zeros([size(bin_x, 2),size(bin_y, 2)]);
sumLRel = zeros([size(bin_x, 2),size(bin_y, 2)]);
n = 0;
mod = 2; %0 diff 1 diffRel 2 MAGAbs 3 MAGREL 4 MAGNorm 

create2D = [0, 9, 1]; % (1) 0/1 -> create 2D or not, (2) [1, 29] -> which ET to use (3) 0/1 -> 0=Abs, 1=Rel
DIFFTHRESH = 0.1;

for i = 1:numel(D)
    file = D(i);
%     file.name = 'Hololens_hall_up3';
%     file.folder = 'X:\CIIRC\BP\benchmark';
    fprintf(" > Starting dataset %s.\n", file.name);
    toMatch = load(fullfile(file.folder, file.name, 'toMatch.mat')).toMatch ;
    for j = 1:size(toMatch,1)
        iA = toMatch(j,1);
        iB = toMatch(j,2);
        fprintf("  >Matching %d-th combination : %d-%d.\n", j, iA, iB);
        
        %get paths
        respath = fullfile('X:\CIIRC\BP\benchmark\results\', file.name);
        dataLoRA = fullfile(respath, sprintf('datI_%d_%d_LoRA_r.mat',iA, iB));
        dataMAG = fullfile(respath, sprintf('datI_%d_%d_MAG.mat',iA, iB));
        
        %load data
        datsL = load(dataLoRA).a;
        datsM = load(dataMAG).itersUpdated;
        if size(datsL, 1) == size(datsM, 1)
            numIters = size(datsL, 1);
        else
            error('Invalid input sizes')
        end
        
        [cntM, cntMRel, cntL, cntLRel] = createHist(datsL, datsM, numIters, bin_x, bin_y);
        itername = sprintf('%s%d', file.name, j);
        
        sumM = sumM + cntM;
        sumMRel = sumMRel + cntMRel;
        sumL = sumL + cntL;
        sumLRel = sumLRel + cntLRel;
        sumMN = sumMN + cntM./max(max(cntM));
        n = n+1;
    end
end

sumM = sumM/n;
sumMRel = sumMRel/n;
sumL = sumL/n;
sumLRel = sumLRel/n;


% Plot frequency values with surf
[x,y] = meshgrid(bin_x,bin_y);

if mod == 0
    d = (sumM-sumL);
    p = d > DIFFTHRESH;
    d(p) = 1;
    n = d < -DIFFTHRESH;
    d(n) = -1;
    z = d <= DIFFTHRESH & d>= -DIFFTHRESH;
    d(z) = 0;
    xBounds = [0,1.025];
    yBounds = [2, 30];
elseif mod == 1
    d = (sumMRel-sumLRel);
    z = d <= DIFFTHRESH & d>= -DIFFTHRESH;
    d(z) = 0;
    p = d > DIFFTHRESH;
    d(p) = 1;
    n = d < -DIFFTHRESH;
    d(n) = -1;
    xBounds = [0,1.025];
    yBounds = [2, 30];
elseif mod == 2
    d = (sumM);
    xBounds = [0,0.725];
    yBounds = [2, 30];
elseif mod == 3
    d = (sumMRel);
    xBounds = [0,0.725];
    yBounds = [2, 30];
else
    d = (sumMN);
    xBounds = [0,0.725];
    yBounds = [2, 30];
end
figure();
hold on;

for i = 1 : size(bin_x, 2)
    for j = 1: size(bin_y, 2)
        pA = bin_x(i);
        pB = bin_y(j);
        if (pA >= xBounds(1)) && (pA < xBounds(2)) && (pB >= yBounds(1)) && (pB < yBounds(2))
            if mod == 0 || mod == 1
                if d(i,j) > 0
                    txt = '+';
                elseif d(i,j) < 0
                    txt = '-';
                else
                    txt = '0';
                end
            elseif mod == 4
                txt = sprintf('%.1f',d(i,j));
            else
                txt = sprintf('%d',uint16(d(i,j)));
            end
            text(pA+0.0025,pB + 0.5,txt, 'FontSize', 11.5,'Fontweight','Bold');
         end
    end
end
   


surf(x,y,d', 'CdataMode','auto');
%hist3(,'CdataMode','auto', 'Edges', {bin_x,bin_y});
view(2)
if mod == 0
    title('Avg. diff. of no. matches for MAGSAC++ and LO-RANSAC', 'Fontweight','Bold')
elseif mod == 1
    title('Avg. diff. of precision for MAGSAC++ and LO-RANSAC', 'Fontweight','Bold')
elseif mod == 2
    title('Avg. number of matches of MAGSAC++', 'Fontweight','Bold')
elseif mod == 3
    title('Avg. precision of MAGSAC++', 'Fontweight','Bold')
else
    title('Avg. norm. number of matches of MAGSAC++', 'Fontweight','Bold')
end

xlabel('Peak threshold');
ylabel('Edge threshold');
s = findobj(gca,'Type','Surface');
s.FaceAlpha = 0.5;
xlim(xBounds)
ylim(yBounds)
%colorbar;
grid on;
set(gca,'FontSize',13);
set(gca,'Linewidth',1.1);
set(gcf,'Position', [100,100,750,600]);

if create2D(1)
    figure(); hold on;
    if ~ create2D(3)
        title('Comaprison of avg. number of matches', 'Fontweight','Bold')
        lineM = sumM(1:320, create2D(2));
        lineL = sumL(1:320, create2D(2));
        ylabel('Avg. num. matches');
    else
        title('Comaprison of avg. precision', 'Fontweight','Bold')
        lineM = sumMRel(:, create2D(2));
        lineL = sumLRel(:, create2D(2));
        ylabel('Avg. precision of matches');
    end
    
    ixs = lineM > 0;
    lineM = lineM(ixs);
    lineL = lineL(ixs);
    t = bin_x(ixs);
    plot(t, movmean(lineM,3),'Linewidth',1.3);
    plot(t, movmean(lineL,3),'Linewidth',1.3);
    xlabel('Peak threshold');
    legend('MAGSAC++','LO-RANSAC');
    set(gca,'FontSize',13);
    set(gca,'Linewidth',1.2);
    set(gcf,'Position', [100,100,750,600]);
    grid on;
end
function [cntM, cntMRel, cntL, cntLRel] = createHist(datsL, datsM, numIters, bin_x, bin_y)
    
    histDataLoRaVerRel = [];
    histDataLoRaVer = [];
    histDataMagVer = [];
    histDataMagVerRel = [];
    
    for i = 1:numIters
        
        pT = cast(datsM{i, 2}, 'double');
        eT = cast(datsM{i, 1}, 'double');
        
        matchesLRV = datsL(i).vMa;
        matchesLR = datsL(i).gMa;
        matchesMG = datsM{i, 3}+1;
        matchesMGV = datsM{i, 4}+1;

        nMGV = size(matchesMGV, 2);
        nLRV = size(matchesLRV, 2);
        nMG = size(matchesMG, 2);
        nLR = size(matchesLR, 2);

        histDataLoRaVer = cast([histDataLoRaVer; repmat([pT, eT],nLRV,1)], 'double');
        histDataMagVer = cast([histDataMagVer; repmat([pT, eT],nMGV,1)], 'double');

        nM = int16(round(nMGV/nMG*100));
        histDataMagVerRel = [histDataMagVerRel; repmat([pT, eT],nM,1)];

        nL = int16(round(nLRV/nLR*100));
        histDataLoRaVerRel = [histDataLoRaVerRel; repmat([pT, eT],nL,1)];

    end
    
    
    cntM = hist3(histDataMagVer,{bin_x,bin_y});
    cntMRel = hist3(histDataMagVerRel,{bin_x,bin_y});
    
    cntL = hist3(histDataLoRaVer,{bin_x,bin_y});
    cntLRel = hist3(histDataLoRaVerRel,{bin_x,bin_y});

end
