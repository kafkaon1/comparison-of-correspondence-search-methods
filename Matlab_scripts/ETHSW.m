
clc; clear all;
ethDir = 'X:\CIIRC\datasets\'; %'X:\CIIRC\BP\ETH3D';
resRootDir = 'X:\CIIRC\datasets\results';%'X:\CIIRC\BP\ETH3D\results';
D = dir(ethDir);
D = D(ismember({D.name}, { 'ap2'}));

fprintf("=> Starting WSI FE and FM for ETH3D.\n");
pkT = 0.31;
egT = 12;
MET = 'WSI';
SCALE = 0.3;
addpath('X:\CIIRC\src\bm3d_matlab_package\bm3d');

for i = 1:numel(D)
    dataset = D(i);
    fprintf(" > Dataset %s.\n", dataset.name);
    imPath = fullfile(ethDir, dataset.name, 'images');
    ims = dir(imPath);
    ims = ims(~ismember({ims.name}, {'.', '..'}));
    
    names = {};
    points = {};
    descs = {};
    resDir = fullfile(resRootDir, dataset.name, MET);
    if ~exist(resDir, 'file')
        mkdir(resDir)
    end

    nims = int16(factorial(numel(ims))/(factorial(numel(ims)-2))/2);
    n = 0;
    for iA = 1:numel(ims)
        imA = ims(iA).name;
   
        
        ixA = strcmpi(imA,names);
        if isempty(ixA)| xor(ixA, ones(size(ixA)))
            imgA = imread(fullfile(imPath, imA));
            sO = size(imgA);
            imgA = imresize(imgA, SCALE);
            fprintf('   Denoising %s.\n', imA)
            imgAd = im2uint8(CBM3D(im2double(imgA), 0.11)); 
            wImgA= WallisFilter(rgb2gray(imgAd), 127, 60, 2.1,0.7, 14, false);
            I1 = single(wImgA);
            [f1,d1] = vl_sift(I1, 'PeakThresh', pkT, 'edgethresh', egT);
            names = [names imA];
            points = [points f1];
            descs = [descs d1];
            
            outFA = sprintf('%s.mat', imA);
            outPA = fullfile(resDir, outFA);
            kpSCALE = sO./size(imgA);
            kp = f1(1:2, :).*[kpSCALE(2); kpSCALE(1)];
            save(outPA, 'kp');
        else
            f1 = points{ixA};
            d1 = descs{ixA};
        end
        
        for iB = iA+1:numel(ims)
            n = n+1;
            imB =  ims(iB).name;
            fprintf('  Iteration %d/%d with %s and %s.\n', n, nims, imA, imB);
            
            outFM = sprintf('%s-%s.mat', imA, imB);
            outPM =  fullfile(resDir, outFM);
            
            if(iA==iB) || isfile(outPM)
                continue
            end
            
            ixB = strcmpi(imB,names);
            if xor(ixB, ones(size(ixB)))
                imgB = imread(fullfile(imPath, imB));
                sO = size(imgB);
                imgB = imresize(imgB, SCALE);
                fprintf('   Denoising %s .\n', imB)
                imgBd = im2uint8(CBM3D(im2double(imgB), 0.11)); 
                wImgB= WallisFilter(rgb2gray(imgBd), 127, 60, 2.1,0.7, 14, false);
                I2 = single(wImgB);
                [f2,d2] = vl_sift(I2, 'PeakThresh', pkT, 'edgethresh', egT);
                names = [names imB];
                points = [points f2];
                descs = [descs d2];
               
                outFB = sprintf('%s.mat', imB);
                outPB = fullfile(resDir, outFB);
                kpSCALE = sO./size(imgB);
                kp = f2(1:2, :).*[kpSCALE(2); kpSCALE(1)];
                save(outPB, 'kp');
            else
                f2 = points{ixB};
                d2 = descs{ixB};
            end
            
            [matches, scores] = vl_ubcmatch(d1, d2, 1.5);
            
            %draw_matches_o(fullfile(imPath, imA),fullfile(imPath, imB), f1(1:2,:).*[kpSCALE(2); kpSCALE(1)], f2(1:2,:).*[kpSCALE(2); kpSCALE(1)], matches)

            save(outPM, 'matches');
        end
    end
            
end

%============WALLIS======================================================================================================
function outputImage = WallisFilter(inputImage, desiredMean, desiredStdDev, Amax, percentage, windowWidth, preSmooth)
outputImage = []; % Initialize
% Make sure the window width is odd.  If it's not, add 1 to make it odd.
if mod(windowWidth, 2) == 0
	% For example, if windowWidth = 4, make it 5 instead.
	windowWidth = windowWidth+1;
end
% Get a double copy of the image.
dblImage = double(inputImage); % Cast to double from whatever it is on input.

% Optionally, smooth the image with a gaussian blur beforehand, if specified.
if preSmooth
	% gauss_blur = 1/273 * [1 4 7 4 1; 4 16 26 16 4; 7 26 41 26 7; 4 16 26 16 4; 1 4 7 4 1];
	gauss_blur = fspecial('gaussian', windowWidth, 1);
	dblImage = conv2(dblImage, gauss_blur, 'same');
end

% Make an image of all ones the same size as the input image.
uniformImage = ones(size(dblImage));
% Make a convolution kernel of all ones to integrate the gray level within the window.
kernel = ones(windowWidth);
% Make a new image where pixel value is the sum of gray levels within the sliding window.
sumImage = conv2(dblImage, kernel, 'same');
% Make a new image where pixel value is the COUNT of pixels within the sliding window.  Will be smaller near the edges of the image.
countImage = conv2(uniformImage, kernel, 'same');

% Get the sliding mean image.
localMeanImage = sumImage ./ countImage;
% Compute the variance image = <(x-xBar).^2> = second central moment.
D = conv2((dblImage - localMeanImage).^2, kernel, 'same') ./ countImage; %(windowWidth^2); %;
% Compute local standard deviation by taking the square root of the variance image.
D = sqrt(D);
% Apply the Wallis formula scaling.

G = (dblImage - localMeanImage) .*  desiredStdDev* Amax  ./ (Amax + D ) + percentage * desiredMean + (1-percentage) * localMeanImage;

%G = (dblImage - localMeanImage) .* Amax .* desiredStdDev ./ (Amax .* D + desiredStdDev) + percentage * desiredMean + (1-percentage) * localMeanImage;

% Clip values.
% Don't let image be less than 0.
G(G < 0) = 0;
% Don't let image be more than the max.
maxPossibleGrayLevel = intmax(class(inputImage));
G(G > maxPossibleGrayLevel) = maxPossibleGrayLevel;

% Cast back to the same class as the input image.
outputImage = cast(G, 'like', inputImage);
end
