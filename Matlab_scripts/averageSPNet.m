clc; clear all;

%dataF = 'X:\\CIIRC\\BP\\benchmark\\results\\HoloLens_door\\i1i38_md.mat';


clc; clear all;
benchdir = 'X:\CIIRC\BP\benchmark';
D = dir(benchdir);
D = D(~ismember({D.name}, {'.', '..', 'results'}));

fprintf("=> Starting average benchmarking.\n");


pcs = linspace(2, 100, 50);
datsA = struct();
datsR = struct();
datsS = struct();
n = 0;
aA = zeros(size(pcs));
aR = zeros( size(pcs));
aN = zeros( size(pcs));

for i = 1:numel(D)
    file = D(i);
    fprintf(" > Starting dataset %s.\n", file.name);
    toMatch = load(fullfile(file.folder, file.name, 'toMatch.mat')).toMatch ;
    for j = 1:size(toMatch,1)
        iA = toMatch(j,1);
        iB = toMatch(j,2);
        
        fprintf("  >Matching %d-th combination : %d-%d.\n", j, iA, iB);
        
        %get paths
        respath = fullfile('X:\CIIRC\BP\benchmark\results\', file.name);
        spMagpath =  fullfile(respath, sprintf('datI_%d_%d_SP_MAG.mat',iA, iB));
        spDefpath = fullfile(respath, sprintf('datI_%d_%d_SP_r.mat',iA, iB));
        %load data
        
        spScores = load(spDefpath).SPmatches{3}(:,3);
        spMag = load(spMagpath).itersUpdated;
        
        if(file.name == "HoloLens_hall_down" && j==3)
            sg = load(spDefpath).SPmatches;
            imA = fullfile(file.folder, file.name, sprintf('img%d.jpg', iA));
            imB = fullfile(file.folder, file.name, sprintf('img%d.jpg', iB));
            kpA = fullfile(file.folder, file.name, sprintf('ktps%d.mat',iA));
            kpB = fullfile(file.folder, file.name, sprintf('ktps%d.mat',iB));
            load(kpA); load(kpB);
            
            addpath('X:\CIIRC\BP\solveryM\relative_pose_solvers');
            idW = 9;
            pc = spMag{idW, 1};
            kpA = sg{1}';
            kpB = sg{2}';
            matches = sg{3}';
            iM = int16(size(matches,2)*pc/100);
            matches = matches(1:2,1:iM)+1;
            gMatches = spMag{idW, 2} +1;
%             correspondences = [kpA(1:2, matches(1,:)); kpB(1:2, matches(2,:))]';
%             gVerMatches = [];
%             if size(matches,2) >= 7
%                 [score7pc, gVerMatches, model7pc] = vanillaFundamentalMatrixRANSAC(correspondences, 0.98, 1, 0);
%                 %gVerMatches = gVerMatches(1,:);%verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON);
%             end
%             
%             gMatches = matches(:,gVerMatches);
%             verMatches = verifyMatches(gMatches, kpA, kpB, pvxA, pvxB, 11);
            draw_matches_v(imA,imB, kpA, kpB,gMatches, gMatches)
            draw_matches_v(imA,imB, kpA, kpB,matches, matches)
            disp('drawn')
        end
        
        [dataAbs, dataRel] = create_plt_dt(spMag);
        [x, yA, yR] = create_plt_sc(spMag, spScores);
        itername = sprintf('%s%d', file.name, j);
        datsA.(itername) = dataAbs;
        datsR.(itername) = dataRel;
        datsSA.(itername) = yA;
        datsSR.(itername) = yR;
        datsSx.(itername) = x;
        aA = aA + dataAbs;
        aR = aR + dataRel;
        aN = aN + dataAbs/max(dataAbs);
        n = n+1;
        
    end
end


figure(); hold on;

plot(pcs, movmean(aN/n,1), 'LineWidth', 1.2)
plot(pcs, movmean(aN/n,6), 'LineWidth', 1.2)

iternames = fieldnames(datsA);
for i=1:numel(iternames)
    nRel = datsR.(iternames{i});
    nH = datsA.(iternames{i});
    nH = nH/max(nH);
    lh = plot(pcs, movmean(nH, 3));
    lh.Color = [1 0 1 0.2];
end


title('Abs. average no. of ver. matches, SPNet', 'Fontweight','Bold')
xlabel('percentage of points used [%]');
ylabel('normalized no. of corr. matches ');
grid on;
set(gca,'FontSize',13);
set(gca,'Linewidth',1.1);
set(gcf,'Position', [100,100,750,600]);

legend('Avg. value', 'Avg. value filtered MA6', 'Single im. pair with MA3','FontSize',12)

figure(); hold on;

plot(pcs, movmean(aR/n,1), 'LineWidth', 1.2)
plot(pcs, movmean(aR/n,6), 'LineWidth', 1.2)

iternames = fieldnames(datsA);
for i=1:numel(iternames)
    nR = datsR.(iternames{i});
    nH = datsA.(iternames{i});
    nH = nH/max(nH);
    lh = plot(pcs, movmean(nR, 3));
    lh.Color = [1 0 1 0.2];
end


legend('Avg. value', 'Avg. value filtered MA6', 'Single im. pair with MA3','FontSize',12)

title('Average precision of ver. matches, SPNet', 'Fontweight','Bold')
xlabel('percentage of points used [%]');
ylabel('precision [%]');
grid on;
set(gca,'FontSize',13);
set(gca,'Linewidth',1.1);
set(gcf,'Position', [100,100,750,600]);

% figure(); hold on;
% 
% iternames = fieldnames(datsSA);
% for i=1:numel(iternames)
%     nSR = datsSR.(iternames{i});
%     t = datsSx.(iternames{i});
%     lh = plot(t,movmean(nSR, 3));
% end
% 
% title('Average precision of ver. matches, SPNet', 'Fontweight','Bold')
% xlabel('percentage of matches used [%]');
% ylabel('Score');
% grid on;
% set(gca,'FontSize',13);
% set(gca,'Linewidth',1.1);
% xlim([0,50])
% set(gcf,'Position', [100,100,750,600]);

function [yA, yR] = create_plt_dt(spMag)
    numIt = size(spMag, 1);
    yA = [];
    yR = [];
    
    for i = 1:numIt
        matchesMG = spMag{i, 2};
        matchesMGV = spMag{i, 3};
        
        nMG = size(matchesMG,2);
        nMGV = size(matchesMGV, 2);
        
        yA = [yA nMGV];
        yR = [yR nMGV/nMG*100];
    end
end


function [x, yA, yR] = create_plt_sc(spMag, scores)
    numIt = size(spMag, 1);
    yA = [];
    yR = [];
    x = [];
    for i = 1:numIt
        scP =  spMag{i, 1};
        matchesMG = spMag{i, 2};
        matchesMGV = spMag{i, 3};
        
        nMG = size(matchesMG,2);
        nMGV = size(matchesMGV, 2);
        iS = int16(size(scores,1)*scP/100)
        scE = scores(iS);
        x = [x scE];
        yA = [yA nMGV];
        yR = [yR nMGV/nMG*100];
    end
end

function draw_matches_v(imA,imB, kptsA, kptsB, matches, mV)

    imgA = imread(imA);
    imgB = imread(imB);
    imgAB = [ imgA imgB];
    
    sA = size(imgA);
    
    figure();
    imshow(imgAB); hold on;


    for i = 1:numel(matches(1,:))
        if isVerified(matches(:,i), mV)
            color = 'g';
        else
            color = 'r';
            continue
        end
        plot(kptsA(1,matches(1,i)), kptsA(2, matches(1,i)), 'b.', 'MarkerSize', 3.9)
        plot((kptsB(1,matches(2,i)) + sA(2)), kptsB(2,matches(2,i)), 'b.', 'MarkerSize', 3.9)
        lp = plot([kptsA(1,matches(1,i)) (kptsB(1,matches(2,i)) + sA(2))], [kptsA(2, matches(1,i)) kptsB(2,matches(2,i))], color, 'LineWidth', 0.4);
        lp.Color = [0 1 0 0.3];
    end
    print('a')
end

function correctMatches = verifyMatches(matches, f1, f2, pvxA, pvxB, EPSILON)
    numCorrMtchs = 0;
    correctA = [];
    correctB = [];
    for i=1:size(matches, 2)
        distsA = (pvxA(1,:)-f1(1,matches(1,i))).^2 + (pvxA(2,:)-f1(2,matches(1,i))).^2;
        distsB = (pvxB(1,:)-f2(1,matches(2,i))).^2 + (pvxB(2,:)-f2(2,matches(2,i))).^2;
        epsA = distsA < EPSILON^2;
        epsB = distsB < EPSILON^2;
        
        k = (epsA == 1 & epsB == 1) ;
        correct = 1;
        if  xor(k, ones(size(k)))
            correct = 0;
        end

        if correct
            % test for duplicate matches
            distsMA = (f1(1,correctA) - f1(1,matches(1,i))).^2 + (f1(2,correctA) - f1(2,matches(1,i))).^2;
            distsMB = (f2(1,correctB) - f2(1,matches(2,i))).^2 + (f2(2,correctB) - f2(2,matches(2,i))).^2;
            epsMA = distsMA < 0.01^2;
            epsMB= distsMB < 0.01^2;
            
            nDuplicate = ~(epsMA == 1 | epsMB == 1);
            if isempty(epsMA) | (nDuplicate)
                numCorrMtchs = numCorrMtchs +1;
                correctA = [correctA matches(1,i)];
                correctB = [correctB matches(2,i)];
            end
        end
    end
    correctMatches = [correctA; correctB];
end 


function isV = isVerified(match, mV)
    [C, ~, ~] = intersect(cast(match, 'int64')',cast(mV, 'int64')' ,'rows');
    if C
        isV = 1;
    else
        isV = 0;
    end
end