clc; clear all;

%dataF = 'X:\\CIIRC\\BP\\benchmark\\results\\HoloLens_door\\i1i38_md.mat';

benchdir = 'X:\CIIRC\BP\benchmark';
D = dir(benchdir);
D = D(~ismember({D.name}, {'.', '..', 'results'}));

fprintf("=> Starting average benchmarking.\n");

n = 0;
xBounds = [5e-6,1.6];
yBounds = [5e-4,1.6];


bin_x = [5e-6, 1e-5,2.8e-5, 5.5e-5, 0.0001, 0.0003, 0.0006, 0.001, 0.003, 0.006, 0.01, 0.03, 0.06, 0.1, 0.3, 0.6, 1, 1.6];
bin_y = [0.0005, 0.001, 0.003, 0.006, 0.01, 0.03, 0.06, 0.1, 0.3, 0.6, 1,1.6];

sumHist = zeros([size(bin_x, 2),size(bin_y, 2)]);
sumHistNv = zeros([size(bin_x, 2),size(bin_y, 2)]);
sumHistR = zeros([size(bin_x, 2),size(bin_y, 2)]);
sumHistNvR = zeros([size(bin_x, 2),size(bin_y, 2)]);
hists = struct();

mod = 1;%0 abs 1 rel 2 absNv 3 RelNv

for i = 1:numel(D)
    file = D(i);
    fprintf(" > Starting dataset %s.\n", file.name);
    toMatch = load(fullfile(file.folder, file.name, 'toMatch.mat')).toMatch ;
    for j = 1:size(toMatch,1)
        iA = toMatch(j,1);
        iB = toMatch(j,2);

        fprintf("  >Matching %d-th combination : %d-%d.\n", j, iA, iB);
        
        %get paths
        respath = fullfile('X:\CIIRC\BP\benchmark\results\', file.name);
        sgMagpath =  fullfile(respath, sprintf('datI_%d_%d_SG_MAG.mat',iA, iB));
        sgPath =  fullfile(respath, sprintf('datI_%d_%d_SG_MAG_a.mat',iA, iB));
        
        %load data
        %sgMag = load(sgMagpath).itersUpdated;
        sgMag = load(sgPath).itersUpdated;
        
        if(file.name == "HoloLens_door")
            iDw = 39;
            sg = load(fullfile(respath, sprintf('datI_%d_%d_SG_r.mat',iA, iB))).itersSG;
            imA = fullfile(file.folder, file.name, sprintf('img%d.jpg', iA));
            imB = fullfile(file.folder, file.name, sprintf('img%d.jpg', iB));
            draw_matches_v(imA,imB, sg{iDw, 3}'.*[1344/640; 756/480], sg{iDw,4}'.*[1344/640; 756/480], sgMag{iDw,3}+1, sgMag{iDw,4}+1)
            draw_matches_v(imA,imB, sg{iDw, 3}'.*[1344/640; 756/480], sg{iDw,4}'.*[1344/640; 756/480], sgMag{iDw,5}+1, sgMag{iDw,6}+1)
        end
        numIters = size(sgMag, 1);
        [histDataMagVer, histDataMagVerRel, histData, histDataRel] = createHist(sgMag, numIters, bin_x, bin_y);
        itername = sprintf('%s%d', file.name, j);
        
        hists.(itername) = histDataMagVer;
        sumHist = sumHist + histDataMagVer;
        sumHistR = sumHistR + histDataMagVerRel;
        sumHistNv = sumHistNv + histData;
        sumHistNvR = sumHistNvR + histDataRel;
        n = n+1;
    end
end

% visualisation

iternames = fieldnames(hists);

avgH = sumHist/n;
avgR = sumHistR/n;
avgNv = sumHistNv/n;
avgNvR = sumHistNvR/n;

% Plot frequency values with surf
[x,y] = meshgrid(bin_x,bin_y);

if mod == 0
    d = (avgH)';
elseif mod == 1
    d = (avgR)';
elseif mod == 2
    d = (avgNv)';
else
    d = (avgNvR)';
end
figure();
hold on;

for i = 1 : size(bin_x, 2)
    for j = 1: size(bin_y, 2)
        pA = bin_x(i);
        pB = bin_y(j);
        if (pA >= xBounds(1)) && (pA < xBounds(2)) && (pB >= yBounds(1)) && (pB < yBounds(2))
            if mod == 0
                txt = sprintf('%d',uint16(avgH(i,j)));
            elseif mod == 1
                txt = sprintf('%d',uint16(avgR(i,j)));
            elseif mod == 2
                txt = sprintf('%d',uint16(avgNv(i,j)));
            else
                txt = sprintf('%d',uint16(avgNvR(i,j)));
            end
            text(pA+pA/25,pB + pB/2.3,txt, 'FontSize', 11);

         end
    end
end
   


surf(x,y,d, 'CdataMode','auto');
%hist3(,'CdataMode','auto', 'Edges', {bin_x,bin_y});
view(2)
if mod == 0
    title('Abs. avg. number of correct matches, SuperGlue + MAGSAC++', 'Fontweight','Bold');
elseif mod == 1
    title('Precision of verif. matches, SuperGlue + MAGSAC++', 'Fontweight','Bold');
elseif mod == 2
    title('Abs. avg. number of correct matches, SuperGlue, NV', 'Fontweight','Bold');
else
    title('Precision of verif. matches SuperGlue, NV', 'Fontweight','Bold');
end
xlabel('point Threshold');
ylabel('match Threshold');
s = findobj(gca,'Type','Surface');
s.FaceAlpha = 0.5;
xlim(xBounds)
ylim(yBounds)
%colorbar;
grid on;
set(gca,'FontSize',13);
set(gca,'Linewidth',1.1);
set(gcf,'Position', [100,100,750,600]);
set(gca, 'XScale', 'log');
set(gca, 'YScale', 'log');




function [cntM, cntMRel, cnt, cntRel] = createHist(sgMag, numIters, bin_x, bin_y)

    histDataMagVer = [];
    histDataMagVerRel = [];
    histData = [];
    histDataRel = [];
    
    for i = 1:numIters
        pA = round(sgMag{i, 1},1, 'significant');
        if (pA ==0)
            pA = 5e-6;
        end
        pB = round(sgMag{i, 2},1, 'significant');
        if (pB ==0)
            pB = 5e-4;
        end
        matchesMG = sgMag{i, 3}+1;
        matchesMGV = sgMag{i, 4}+1;
        matches = sgMag{i, 5}+1;
        matchesV = sgMag{i, 6}+1;

        nMGV = size(matchesMGV, 2);
        nMG = size(matchesMG, 2);
        
        
        
        nMt = size(matches, 2);
        nMtV = size(matchesV,2);

        histDataMagVer = cast([histDataMagVer; repmat([pA, pB],nMGV,1)], 'double');
        histData = cast([histData; repmat([pA, pB],nMtV,1)], 'double');

        nM = int16(round(nMGV/nMG*100));
        histDataMagVerRel = [histDataMagVerRel; repmat([pA, pB],nM,1)];
        
        nt =  int16(round(nMtV/nMt*100));
        histDataRel = [histDataRel ; repmat([pA, pB],nt,1)];
    end
    
    cntM = hist3(histDataMagVer,{bin_x,bin_y});
    cntMRel = hist3(histDataMagVerRel,{bin_x,bin_y});

    cnt = hist3(histData,{bin_x,bin_y});
    cntRel = hist3(histDataRel,{bin_x,bin_y});

end

function draw_matches_v(imA,imB, kptsA, kptsB, matches, mV)

    imgA = imread(imA);
    imgB = imread(imB);
    imgAB = [ imgA imgB];
    
    sA = size(imgA);
    
    figure();
    imshow(imgAB); hold on;


    for i = 1:numel(matches(1,:))
        if isVerified(matches(:,i), mV)
            color = 'g';
        else
            color = 'r';
            continue
        end
        plot(kptsA(1,matches(1,i)), kptsA(2, matches(1,i)), 'b.', 'MarkerSize', 3.9)
        plot((kptsB(1,matches(2,i)) + sA(2)), kptsB(2,matches(2,i)), 'b.', 'MarkerSize', 3.9)
        l = plot([kptsA(1,matches(1,i)) (kptsB(1,matches(2,i)) + sA(2))], [kptsA(2, matches(1,i)) kptsB(2,matches(2,i))], color, 'LineWidth', 0.4);
        l.Color = [0 1 0 0.3];
    end
    print('a')
end

function isV = isVerified(match, mV)
    [C, ~, ~] = intersect(cast(match, 'int64')',cast(mV, 'int64')' ,'rows');
    if C
        isV = 1;
    else
        isV = 0;
    end
end